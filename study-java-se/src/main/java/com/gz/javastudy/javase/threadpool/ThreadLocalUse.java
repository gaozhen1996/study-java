package com.gz.javastudy.javase.threadpool;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class ThreadLocalUse {
    static final ThreadLocal<String> threadParam = new ThreadLocal<>();

    public static void main(String[] args) throws InterruptedException {
        thread();
    }

    public static void threadPool() throws InterruptedException {
        //固定池内只有存活3个线程
        ExecutorService execService = Executors.newFixedThreadPool(3);
        //死循环几次才能看出效果
        while (true) {
            Thread t = new Thread(()->{
                if(threadParam.get()==null){
                    threadParam.set("======>>>>>>>>T1");
                }
                System.out.println("t1:" + threadParam.get());
//                threadParam.remove();
            });
            execService.execute(t);
            TimeUnit.SECONDS.sleep(1);
            Thread t2 = new Thread(()-> {
                if(threadParam.get()==null){
                    threadParam.set("======>>>>>>>>>>>>>>T2");
                }
                System.out.println("t2:" + threadParam.get());
//                threadParam.remove();
            });
            execService.execute(t2);
        }
    }

    public static void thread() throws InterruptedException {
        while (true) {
            //线程1
            new Thread(() -> {
                //设置参数
                if(threadParam.get()==null){
                    threadParam.set("======>>>>>>>>T1");
                }
                System.out.println("t1:" + threadParam.get());
            }).start();
            TimeUnit.SECONDS.sleep(1);
            new Thread(() -> {
                //设置参数
                if(threadParam.get()==null){
                    threadParam.set("======>>>>>>>>>>>>>>T2");
                }
                System.out.println("t2:" + threadParam.get());
            }).start();
        }
    }
}

