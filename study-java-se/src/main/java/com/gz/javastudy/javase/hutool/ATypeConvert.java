package com.gz.javastudy.javase.hutool;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.lang.Console;
import cn.hutool.core.lang.Dict;


public class ATypeConvert {
    public static void main(String[] args) {
        Dict dict = Dict.create().set("age",26).set("name","jack").set("create",DateUtil.date());
        int age = dict.getInt("age");
        String name = dict.getStr("name");
        Console.log("age:{} name:{}",age,name);
    }

    public static void convert() {
        //字符串转数字
        System.out.println(Convert.toInt("10", -1));
        //字符串转日期
        System.out.println(Convert.toDate("2023年7月5日"));
        //数字转中文
        System.out.println(Convert.numberToChinese(12345, true));
        //数字转中文金额
        System.out.println(Convert.digitToChinese(123456));
        //获取当前时间
        System.out.println(DateUtil.date());
    }
}
