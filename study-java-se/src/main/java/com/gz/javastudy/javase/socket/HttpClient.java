package com.gz.javastudy.javase.socket;


import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpClient {
    public static void main(String[] args) throws Exception {
        // 目标URL
        String url = "http://127.0.0.1:8087/EX/FMServer/Parsej";
        // 创建连接对象
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        // 设置POST请求
        con.setDoOutput(true);
        con.setRequestMethod("POST");

        // 添加请求头信息（如果需要）
        con.setRequestProperty("Content-Type", "application/json");

        // 开始写入数据到输出流中
        OutputStreamWriter writer = new OutputStreamWriter(con.getOutputStream());
        writer.write("{\"FUNNAM\":\"CHGBNKACC\"}");
        writer.flush();
        writer.close();

        // 获取服务器返回结果
        int responseCode = con.getResponseCode();
        BufferedReader reader = new BufferedReader(new InputStreamReader(con.getInputStream()));
        StringBuilder result = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            result.append(line).append("\n");
        }
        reader.close();

        System.out.println("Response Code : " + responseCode);
        System.out.println("Result : " + result.toString());
    }
}
