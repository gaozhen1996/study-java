package com.gz.javastudy.rocketmq;


import com.gz.javastudy.rocketmq.toolkit.SpringUtil;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import org.springframework.context.annotation.ComponentScan;

/**
 * @author gaozhen
 */
@SpringBootApplication
@ComponentScan("com.gz.javastudy.rocketmq")
public class RocketmqApplication {

    public static void main(String[] args) {
        ApplicationContext con = SpringApplication.run(RocketmqApplication.class, args);
        SpringUtil.setApplicationContext(con);
    }
}
