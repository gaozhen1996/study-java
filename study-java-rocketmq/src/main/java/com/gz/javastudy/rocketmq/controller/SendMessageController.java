package com.gz.javastudy.rocketmq.controller;

import com.alibaba.fastjson.JSONObject;
import com.gz.javastudy.rocketmq.config.Constant;
import com.gz.javastudy.rocketmq.config.RocketMqHelper;
import com.gz.javastudy.rocketmq.domain.Account;
import com.gz.javastudy.rocketmq.domain.MsgDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SendMessageController {

    @Autowired
    private RocketMqHelper rocketMqHelper;

    @GetMapping("send")
    public String testProducter(@RequestParam String account, @RequestParam double balance) {
        Account para = new Account();
        para.setAccount(account);
        para.setBalance(balance);

        MsgDTO msgDTO = new MsgDTO("account",para);
//        rocketMqHelper.asyncSend(Constant.TOPIC,MessageBuilder.withPayload(msgDTO).build(),null,600);
        rocketMqHelper.syncSend(Constant.TOPIC, MessageBuilder.withPayload(msgDTO).build());

        return "消息成功发送到MQ";
    }

}
