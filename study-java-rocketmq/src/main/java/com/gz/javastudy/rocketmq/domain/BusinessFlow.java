package com.gz.javastudy.rocketmq.domain;

import java.io.Serializable;

/**
 * @author gaozhen
 */
public class BusinessFlow implements Serializable {

    private int id;

    private String flowNo;

    private String busType;

    private String account;

    private double amount;

    private String status;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFlowNo() {
        return flowNo;
    }

    public void setFlowNo(String flowNo) {
        this.flowNo = flowNo;
    }

    public String getBusType() {
        return busType;
    }

    public void setBusType(String busType) {
        this.busType = busType;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "BusinessFlow{" +
                "id=" + id +
                ", flowNo='" + flowNo + '\'' +
                ", busType='" + busType + '\'' +
                ", account='" + account + '\'' +
                ", amount=" + amount +
                ", status='" + status + '\'' +
                '}';
    }
}
