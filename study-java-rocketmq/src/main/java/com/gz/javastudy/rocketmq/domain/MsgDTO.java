package com.gz.javastudy.rocketmq.domain;

public class MsgDTO {
    private String type;

    private Object body;

    public MsgDTO(String type, Object body) {
        this.type = type;
        this.body = body;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Object getBody() {
        return body;
    }

    public void setBody(Object body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return "{" +
                "type='" + type + '\'' +
                ", body=" + body +
                '}';
    }
}
