package com.gz.javastudy.rocketmq.domain;

import java.io.Serializable;

/**
 * @author gaozhen
 */
public class Account implements Serializable {

    private String account;

    private double balance;

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    @Override
    public String toString() {
        return "Account{" +
                "account='" + account + '\'' +
                ", balance=" + balance +
                '}';
    }
}
