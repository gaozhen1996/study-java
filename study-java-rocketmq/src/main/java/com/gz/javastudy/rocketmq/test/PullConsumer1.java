package com.gz.javastudy.rocketmq.test;

import org.apache.rocketmq.client.consumer.DefaultMQPullConsumer;
import org.apache.rocketmq.client.consumer.PullResult;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.common.message.MessageQueue;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;


public class PullConsumer1 {
    private  static  final Map<MessageQueue,  Long> OFFSET_TABLE  =  new HashMap<MessageQueue, Long>();

    public static void main(String[] args) throws MQClientException {
        DefaultMQPullConsumer  consumer  =  new DefaultMQPullConsumer("gz-rocketmq-group");
        consumer.setNamesrvAddr("81.68.251.109:19876");
        consumer.start();
        Set<MessageQueue> mqs = consumer.fetchSubscribeMessageQueues("TOPIC");
        for (MessageQueue mq : mqs) {
            long offset = consumer.fetchConsumeOffset(mq, true);
            System.out.printf("Consume from the Queue: " + mq + "%n");
            try {
                System.out.println("当前获取的消息的归属队列是: " + mq.getQueueId());
                PullResult pullResult = consumer.pullBlockIfNotFound(mq, null, getMessageQueueOffset(mq), 32);
                System.out.printf("%s%n", pullResult);
                putMessageQueueOffset(mq,  pullResult.getNextBeginOffset());
                switch (pullResult.getPullStatus()) {
                    case FOUND:
                        List<MessageExt> messageExtList = pullResult.getMsgFoundList();
                        for (MessageExt m : messageExtList){
                            System.out.println(new String(m.getBody()));
                        }
                        break;
                    case NO_MATCHED_MSG:
                        break ;
                    case NO_NEW_MSG:
                        break ;
                    case OFFSET_ILLEGAL:
                        break ;
                    default:
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        consumer.shutdown();
    }
    private static long getMessageQueueOffset(MessageQueue mq) {
        Long Offset = OFFSET_TABLE.get(mq);
        if (Offset != null){
            return Offset;
        }
        return 0;
    }
    private static void putMessageQueueOffset(MessageQueue mq, long Offset) {
        OFFSET_TABLE.put(mq, Offset);
    }
}