package com.gz.javastudy.rocketmq.test;

import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.remoting.common.RemotingHelper;


public class SyncProducer {
    public static void main(String[] args) throws Exception {
        DefaultMQProducer producer = new DefaultMQProducer("gz-rocketmq-group");
        producer.setNamesrvAddr("81.68.251.109:19876");
        producer.start();

        for (int i = 0;i<3;i++){
            Message msg = new Message("TOPIC",("-------Hello,RocketMQ! num:"+i).getBytes(RemotingHelper.DEFAULT_CHARSET));
            SendResult sendResult = producer.send(msg);
            System.out.printf("%s%n",sendResult);

        }
        producer.shutdown();
    }
}
