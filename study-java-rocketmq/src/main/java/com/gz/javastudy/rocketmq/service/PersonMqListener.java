package com.gz.javastudy.rocketmq.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.gz.javastudy.rocketmq.config.Constant;
import com.gz.javastudy.rocketmq.domain.Account;
import com.gz.javastudy.rocketmq.domain.MsgDTO;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;



@Component
@RocketMQMessageListener(consumerGroup = "${rocketmq.producer.groupName}", topic = Constant.TOPIC)
public class PersonMqListener implements RocketMQListener<JSONObject> {
    @Override
    public void onMessage(JSONObject msg) {
        MsgDTO msgDTO = msg.toJavaObject(MsgDTO.class);
        Account account = JSONObject.parseObject(JSON.toJSONString(msgDTO.getBody()), Account.class);
        System.out.println("接收到消息，开始消费:" +account);
    }


}
