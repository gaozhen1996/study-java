package com.gz.javastudy.tomcat.ex02.pyrmont;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

public class RequestTask implements  Runnable{

    private Socket socket;
    private InputStream input;
    private OutputStream output;
    private Request request;
    private Response response;

    public RequestTask(Socket socket){
        try {
            this.socket = socket;
            input = socket.getInputStream();
            output = socket.getOutputStream();
            //创建一个Request和解析
            request = new Request(input);
            request.parse();
            //创建一个Response对象
            response = new Response(output);
            response.setRequest(request);



        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public void run() {
        try {
            //空uri直接return
            if (request.getUri()==null){
                return;
            }
            //判断请求的是静态资源还是Servlet
            if(request.getUri().startsWith("/servlet")){
                ServletProcessor1 processor1 = new ServletProcessor1();
                processor1.process(request,response);
            }else{
                StaticResourceProcessor staticResourceProcessor =
                        new StaticResourceProcessor();
                staticResourceProcessor.process(request,response);
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            //关闭socket
            try {
                if (socket!=null){
                    socket.close();
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    public InputStream getInput() {
        return input;
    }

    public Request getRequest() {
        return request;
    }
}
