package com.gz.javastudy.tomcat.ex02.pyrmont;



import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


/**
 * 等待HTTP请求，为每个请求创建Request和Response对象，
 * 更加HTTP请求的是静态资源还是servlet,将改HTTP请求分发给
 * 一个StaticResourceProcessor实例或者一个ServletProcessor实例。
 */
public class HttpServer1 {
    /**
     * shutdown 命令
     */
    private static final String SHUTDOWN_COMMAND = "/SHUTDOWN";

    /**
     * the shutdown command received
     */
    private boolean shutdown = false;

    public static void main(String[] args) throws Exception {
        HttpServer1 httpServer = new HttpServer1();
        System.out.println("服务启动");
        httpServer.await();
    }

    public void await(){
        ServerSocket serverSocket = null;
        int port = 8080;
        try{
            serverSocket = new ServerSocket(port,1, InetAddress.getByName("127.0.0.1"));
        }catch (IOException e){
            e.printStackTrace();
        }

        /**
         * 创建线程池
         */
        ExecutorService executor = Executors.newFixedThreadPool(200);
        while(!shutdown){
            Socket socket;
            try{
                socket = serverSocket.accept();
                RequestTask task = new RequestTask(socket);
                executor.submit(task);
                //是否是关闭请求
                Request request = task.getRequest();
                String uri = request.getUri();
                if(uri!=null){
                    shutdown = request.getUri().equals(SHUTDOWN_COMMAND);
                }
            }catch (Exception e){
                e.printStackTrace();
                continue;
            }
        }
    }

}
