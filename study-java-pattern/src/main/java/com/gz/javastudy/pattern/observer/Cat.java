package com.gz.javastudy.pattern.observer;

public class Cat implements ICallbackService{

	@Override
	public void callback() {
		System.out.println("cat...");
	}

}
