package com.gz.javastudy.pattern.observer;

public class Dog implements ICallbackService{

	@Override
	public void callback() {
		System.out.println("dog...");
	}

}
