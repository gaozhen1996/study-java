package com.gz.javastudy;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class TestPython {
    public static void main(String [] args) {
        String base = TestPython.class.getResource("").getPath().replace("/target/classes/","/src/main/java/");
        base = base.substring(base.indexOf(":")+1);
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int c = scanner.nextInt();

        try {
            String[] my_args =new String[] {"python",base+"script.py",String.valueOf(a),String.valueOf(b),String.valueOf(c)};
            Process proc =  Runtime.getRuntime().exec(my_args);//执行脚本

            BufferedReader reader = new BufferedReader(new InputStreamReader(proc.getInputStream()));
            String line = null;
            while((line = reader.readLine()) != null){
                System.out.println("======>"+line);
            }

            // 读取Python函数的错误输出
            BufferedReader err = new BufferedReader(new InputStreamReader(proc.getErrorStream()));
            while ((line = err.readLine()) != null) {
                System.err.println("Python function error: " + line);
            }
            reader.close();
            proc.waitFor();
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        } catch (InterruptedException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
}
