package com.gz.javastudy;

import com.github.plexpt.chatgpt.Chatbot;
import com.github.plexpt.chatgpt.Config;

import java.util.Map;

/**
 * @author gaozhen
 */
public class TestGpt {
    public static void main(String[] args) {
        Config config = new Config("awan_note@outlook.com","12345678","","","");
        Chatbot chatbot = new Chatbot(config);
        Map<String, Object> chatResponse = chatbot.getChatResponse("hello");
        System.out.println(chatResponse.get("message"));
    }
}
