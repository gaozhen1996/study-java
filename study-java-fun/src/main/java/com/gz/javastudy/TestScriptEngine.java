package com.gz.javastudy;


import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * @author gaozhen
 */
public class TestScriptEngine {
    public static void main(String[] args) throws ScriptException {
        func();
    }

    public static void simple() throws ScriptException {
        ScriptEngineManager mgr = new ScriptEngineManager();
        ScriptEngine engine = mgr.getEngineByName("JavaScript");
        String foo = "40*2";
        System.out.println(engine.eval(foo));
    }

    public static void func() throws ScriptException {
        String base = TestPython.class.getResource("").getPath().replace("/target/classes/","/src/main/java/");
        base = base.substring(base.indexOf(":")+1);
        String  js = readFile(base+"/script.js");

        ScriptEngineManager mgr = new ScriptEngineManager();
        ScriptEngine engine = mgr.getEngineByName("JavaScript");
        engine.put("input", "a:a1\nb:b1\nc:\nd:d1");
        engine.eval(js);
        String name = (String) engine.get("output");
        System.out.println(name);
    }

    public static String readFile(String path){
        StringBuilder text = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new FileReader(path))) {
            String line;
            while ((line = reader.readLine()) != null) {
                text.append(line+"\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return text.toString();
    }


}
