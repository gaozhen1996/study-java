package com.gz.javastudy.sellticket.config;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.stereotype.Component;

@Component
public class RedisConfig {

    private RedissonClient redissonClient;

    public RedissonClient getRedissonClient() {
        synchronized (this) {
            if(redissonClient==null) {
                Config config = new Config();
                //主从redis
                config.useMasterSlaveServers().setMasterAddress("redis://127.0.0.1:6379")
                        .addSlaveAddress("redis://127.0.0.1:6380");
                //集群redis
//                config.useClusterServers().setScanInterval(200).addNodeAddress("redis://127.0.0.1:6379","redis://127.0.0.1:6380");
                //单机redis
//                config.useSingleServer().setAddress("redis://127.0.0.1:6379");
                RedissonClient redisson = Redisson.create(config);
                this.redissonClient = redisson;
            }
        }
        return this.redissonClient;
    }

}