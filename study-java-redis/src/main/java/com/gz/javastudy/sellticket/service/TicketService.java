package com.gz.javastudy.sellticket.service;

import com.gz.javastudy.sellticket.config.RedisConfig;
import org.redisson.api.RLock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gz.javastudy.sellticket.domain.Ticket;
import com.gz.javastudy.sellticket.repository.TicketDao;

@Service
public class TicketService {
	
	@Autowired
	private TicketDao dao;

	@Autowired
	private RedisConfig redisConfig;

	public Ticket getTicket() {
		return dao.selectTicket().get(0);
	}
	
	public int updateTicket(int num) {
		Ticket ticket = new Ticket();
		ticket.setId(num);
		return dao.updateTicket(ticket);
	}
	
	public int sellByLock() {
		RLock lock = redisConfig.getRedissonClient().getLock("ticket");
		int currNum = 0;
		try {
			lock.lock();
			currNum = this.getTicket().getId();
			if(currNum>0) {
				int temp = currNum-1;
				this.updateTicket(temp);
			}
		}finally {
			lock.unlock();
		}
		return currNum;
	}


	public int sellByNoLock() {
		int currNum = 0;
		currNum = this.getTicket().getId();
		if(currNum>0) {
			int temp = currNum - 1;
			this.updateTicket(temp);
		}
		return currNum;
	}

}
