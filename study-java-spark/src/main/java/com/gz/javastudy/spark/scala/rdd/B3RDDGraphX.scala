package com.gz.javastudy.spark.scala.rdd

import org.apache.spark.graphx.{Edge, Graph}
import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object B3RDDGraphX {

  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setAppName("MyScalaWordCount").setMaster("local");
    val sc = new SparkContext(conf)
    //1.创建顶点集合
    val vertexArray = Array(
      (1L, ("ALice", 30)),
      (2L, ("Henry", 27)),
      (3L, ("Charlie", 25)),
      (4L, ("Peter", 22)),
      (5L, ("Henry", 29)),
      (6L, ("Kate", 23))
    )
    //创建边集合
    val edgeArray = Array(
      Edge(2L, 1L, "关注"),
      Edge(2L, 4L, "喜欢"),
      Edge(3L, 2L, "关注"),
      Edge(3L, 6L, "关注"),
      Edge(5L, 2L, "喜欢"),
      Edge(5L, 3L, "关注"),
      Edge(5L, 6L, "关注"),
    )
    //2.构造顶点RDD和边RDD
    val vertexRDD: RDD[(Long, (String, Int))] = sc.parallelize(vertexArray)
    val edgeRDD: RDD[Edge[String]] = sc.parallelize(edgeArray)
    //3.构造Graphx图
    val graph: Graph[(String, Int), String] = Graph(vertexRDD, edgeRDD)
    graph.vertices.filter { case (id, (name, age)) => age > 25 }.collect.foreach {
      case (id, (name, age)) => println(s"$name is $age")
    }

    for (triplet <- graph.triplets.collect) {
      println(s"${triplet.srcAttr._1} ${triplet.attr} ${triplet.dstAttr._1}")
    }
  }
}
