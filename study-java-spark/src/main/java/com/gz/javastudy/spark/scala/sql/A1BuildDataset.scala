package com.gz.javastudy.spark.scala.sql

import org.apache.spark.sql.{Dataset, Encoders, SparkSession}

object A1BuildDataset {
  def main(args: Array[String]): Unit = {
    println("1.创建Spark上下文")
    val spark = SparkSession.builder.appName("Dataset1").master("local[*]").getOrCreate

    println("2.将文件加载为Dataset")
    val d1 = spark.read.textFile("study-java-spark/data/person.txt")
    d1.show()

    println("3.Dataset转化为Person类型")
    import spark.implicits._//导入隐式转换
    val personDataset:Dataset[Person]=d1.map(line=>{
      val fields = line.split(",")
      val id = fields(0).toInt
      val name = fields(1)
      val age = fields(2).toInt
      Person(id, name, age)
    })
    personDataset.show()

    println("4.Dataset转化为DataFrame类型,创建视图v_person,select * from v_person order by age desc")
    val personDataFrame = personDataset.toDF()
    personDataset.createTempView("v_person")
    val result = spark.sql("select * from v_person order by age desc")
    result.show()
  }
  case class Person(id:Int,name:String,age:Int)
}

