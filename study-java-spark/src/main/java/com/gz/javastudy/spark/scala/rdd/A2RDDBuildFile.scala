package com.gz.javastudy.spark.scala.rdd

import org.apache.spark.{SparkConf, SparkContext}

object A2RDDBuildFile {

  def main(args: Array[String]): Unit = {
    /**
     * 1.准备环境
     */
    val sparkConf = new SparkConf().setMaster("local[*]").setAppName("RDD")
    val sc = new SparkContext(sparkConf)
    /**
     * 2.从文件中创建RDD，将文件的数据作为处理的数据源
     */
    val rdd = sc.textFile("study-java-spark/data/a.txt")

    /**
     * 3.输出
     */
    rdd.collect().foreach(println)

    sc.stop()
  }
}
