package com.gz.javastudy.spark.scala

import org.apache.spark.{SparkConf, SparkContext}

object A1WordCountByFile {
  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setAppName("MyScalaWordCount").setMaster("local");

    //创建一个SparkContext对象
    val sc = new SparkContext(conf)

    //执行WordCount
    val result = sc.textFile("data/a.txt").flatMap(_.split(" ")).map((_, 1)).reduceByKey(_ + _)
    //打印在屏幕上
    result.foreach(println)
    //释放资源
    sc.stop()
  }
}
