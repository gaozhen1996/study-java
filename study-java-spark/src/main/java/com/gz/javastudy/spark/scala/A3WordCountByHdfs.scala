package com.gz.javastudy.spark.scala

import org.apache.spark.{SparkConf, SparkContext}

object A3WordCountByHdfs {
  def main(args: Array[String]): Unit = {

    val path = "hdfs://127.0.0.1:9000/input/a.txt"
    val hdfsTarget = "hdfs://127.0.0.1:9000/output/a_result.txt"

    val conf = new SparkConf()
      .setAppName("MyScalaWordCount")
//      .setMaster("local[*]")
      .setMaster("yarn")
    //创建一个SparkContext对象
    val sc = new SparkContext(conf)
    //执行WordCount
    val result = sc.textFile(path).flatMap(_.split(" ")).map((_, 1)).reduceByKey(_ + _)
    //打印在屏幕上
    result.foreach(println)
    result.saveAsTextFile(hdfsTarget)
    //释放资源
    sc.stop()
  }
}
