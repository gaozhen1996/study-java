package com.gz.javastudy.spark.zdcapp


import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.spark.SparkConf
import org.apache.spark.streaming.dstream.{DStream, InputDStream}
import org.apache.spark.streaming.kafka010.ConsumerStrategies.Subscribe
import org.apache.spark.streaming.kafka010.{KafkaUtils, LocationStrategies}
import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.apache.spark.sql.{DataFrame, SparkSession}

object TestKafka {

  def main(args: Array[String]): Unit = {


    val conf = new SparkConf().setMaster("local[*]").setAppName("StreamingKafkaWordCount")

    val ssc = new StreamingContext(conf,Seconds(1))
    val  kafkaTopics = Array("topic_test")

    val kafkaParams = Map[String,Object](
      "bootstrap.servers" ->"81.68.251.109:9092",
      "key.deserializer" -> classOf[StringDeserializer],
      "value.deserializer" -> classOf[StringDeserializer],
      "group.id" -> "1",
      "enable.auto.commit" -> (false: java.lang.Boolean)
    )

    val  inputStream: InputDStream[ConsumerRecord[String,String]] =
      KafkaUtils.createDirectStream[String,String](
        ssc,
        LocationStrategies.PreferConsistent,
        Subscribe[String,String](kafkaTopics,kafkaParams)
      )

    val linesDStream = inputStream.map(record => (record.key, record.value))
    val wordDStream = linesDStream.map(_._2)
    wordDStream.foreachRDD(rdd =>{
      //利用SparkConf来初始化SparkSession。
      val sparkSession: SparkSession = SparkSession.builder().config(conf).getOrCreate()

      //导入隐式转换来将RDD
      import sparkSession.implicits._
      //将RDD转换成DF
      val df: DataFrame = rdd.toDF("value")
      df.createOrReplaceTempView("compute")
      val computeDF: DataFrame = sparkSession.sql("select * from compute")
      computeDF.show()
    })

    ssc.start()
    ssc.awaitTermination()

  }



}
