package com.gz.javastudy.spark.scala.rdd

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

import java.util.Date

object C1RDDTopN {
  def main(args: Array[String]): Unit = {
    //1.创建Spark上下文
    val conf = new SparkConf()
    conf.setAppName("RDDGroupTopN")
    conf.setMaster("local[*]")
    val sc = new SparkContext(conf)
    //2.读取数据文件
    val linesRDD : RDD[String] = sc.textFile("study-java-spark/data/score.txt")
    //3.使用map()算子将linesRDD的元素类型转为(姓名，成绩)形式的元组，便于后续的聚合统计
    val tupleRDD : RDD[(String,Int)]= linesRDD.map(line=>{
        val name = line.split(",")(0)
        val score = line.split(",")(1)
      (name,score.toInt)
    })
    //4.使用groupByKey()算子将tupleRDD按照key（姓名）进行分组，
    //姓名相同的所有成绩数据将聚合到一起；然后使用map()算子将分组后的每一组成绩数据降序排列后取前3个
    val top3 =tupleRDD.groupByKey().map(groupedData=>{
      val name =groupedData._1 //姓名
      val scoreTop3:List[Int]=groupedData._2//成绩集合
        .toList.sortWith(_>_).take(3)//降序取前3个
      (name,scoreTop3)
    })
    //5.输出结果，不加上collect在多线程环境可能会乱序
    top3.collect().foreach(line => {
      println("姓名："+line._1)
      line._2.foreach(score=>{
        println("成绩："+ score)
      })
      println("****************************")
    })

  }

  case class RegisterUser(date:Date,name:String,age:Int)

}
