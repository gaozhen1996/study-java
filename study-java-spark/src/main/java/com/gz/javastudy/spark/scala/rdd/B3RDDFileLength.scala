package com.gz.javastudy.spark.scala.rdd

import org.apache.spark.{SparkConf, SparkContext}

object B3RDDFileLength {
  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setAppName("MyScalaWordCount").setMaster("local");
    //创建一个SparkContext对象
    val sc = new SparkContext(conf)
    val lines = sc.textFile("study-java-spark/data/a.txt")
    val lineLengths = lines.map(s => s.length)
    lineLengths.foreach(a => println(a))
  }
}
