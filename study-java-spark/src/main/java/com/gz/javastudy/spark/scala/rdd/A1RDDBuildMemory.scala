package com.gz.javastudy.spark.scala.rdd

import org.apache.spark.{SparkConf, SparkContext}

object A1RDDBuildMemory {

  def main(args: Array[String]): Unit = {
    /**
     * 1.准备环境
     */
    val sparkConf = new SparkConf().setMaster("local[*]").setAppName("RDD")
    val sc = new SparkContext(sparkConf)
    /**
     * 2.从内存中创建RDD，将内存中集合的数据作为处理的数据源
     */
    val seq = Seq[Int](1,2,3,4)
    // parallelize:并行
    // val rdd = sc.parallelize(seq)
    val rdd = sc.makeRDD(seq)

    /**
     * 3.输出
     */
    rdd.collect().foreach(println)

    sc.stop()
  }
}
