package com.gz.javastudy.spark.scala

import org.apache.spark.streaming.dstream.{DStream, ReceiverInputDStream}
import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.apache.spark.{SparkConf, SparkContext}

object A2WordCountBySocket {
  def main(args: Array[String]): Unit = {
    //获取SparkConf
    val sparkConf: SparkConf = new SparkConf().setAppName("Streaming_WordCountTest").setMaster("local[*]")
    //获取SparkContext
    val sparkContext: SparkContext = new SparkContext(sparkConf)
    //设置日志级别
    sparkContext.setLogLevel("WARN")

    //获取StreamingContext  需要两个参数 SparkContext和duration，后者就是间隔时间
    val streamContext: StreamingContext = new StreamingContext(sparkContext, Seconds(5))

    //从socket获取数据
    val stream: ReceiverInputDStream[String] = streamContext.socketTextStream("localhost", 9999)

    //对数据进行计数操作
    val result: DStream[(String, Int)] = stream.flatMap(x => x.split(" ")).map((_, 1)).reduceByKey(_ + _)
    //输出数据
    result.print()

    //启动程序
    streamContext.start()
    streamContext.awaitTermination()
  }
}
