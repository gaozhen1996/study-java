package com.gz.javastudy.spark.java;

import java.util.Arrays;
import java.util.List;
import org.apache.spark.sql.SparkSession;
import scala.Tuple2;

public final class WordCountByFile {

    public static void main(String[] args) throws Exception {
        String file = "study-java-spark/data/a.txt";

        SparkSession spark = SparkSession.builder().appName("JavaWordCount").master("local[*]").getOrCreate();
        List<Tuple2<String, Integer>> output =
                spark.read().textFile(file).javaRDD()
                .flatMap(line -> Arrays.asList(line.split(" ")).iterator())
                .mapToPair(w -> new Tuple2<String, Integer>(w, 1))
                .reduceByKey((x, y) -> x + y)
                .collect();
        for (Tuple2<?, ?> tuple : output) {
            System.out.println(tuple._1() + "---> " + tuple._2());
        }

        spark.stop();
    }
}