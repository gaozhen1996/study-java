package com.gz.javastudy.spark.scala.sql

import org.apache.spark.sql.{Dataset, Encoders, SparkSession}
import java.text.SimpleDateFormat



object C1新增用户统计 {

  val sdf =  new SimpleDateFormat( "yyyy-MM-dd" );

  def main(args: Array[String]): Unit = {
    println("1.创建Spark上下文")
    val spark = SparkSession.builder.appName("Dataset1").master("local[*]").getOrCreate

    println("2.将文件加载为Dataset")
    val d1 = spark.read.textFile("study-java-spark/data/新增用户日志.log")
    d1.show()

    println("3.Dataset转化为类型")
    val ds:Dataset[RegisterUser]=d1.map(line=>{
      println(line)
      val fields = line.split(",")
      val date = fields(0)
      val name = fields(1)
      RegisterUser(date, name)
    })(Encoders.product[RegisterUser])
    ds.show()

    println("4.Dataset转化为DataFrame类型,创建视图v_register_user,执行SQL")
    val df = ds.toDF()
    df.createTempView("v_register_user")
    val result = spark.sql("select * from (select t.date,count(*) cnt from v_register_user t  group by t.date) t order by cnt desc")
    result.show()
  }

  case class RegisterUser(date:String, name:String)

}
