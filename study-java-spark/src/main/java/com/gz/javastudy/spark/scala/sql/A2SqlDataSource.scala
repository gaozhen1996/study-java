package com.gz.javastudy.spark.scala.sql

import org.apache.spark.sql.SparkSession

object A2SqlDataSource {

  def main(args: Array[String]): Unit = {
    println("1.创建Spark上下文")
    val spark = SparkSession.builder.appName("Dataset1").master("local[*]").getOrCreate

    println("2.加载JSON文件")
    val personDataFrame = spark.read.format("json").load("study-java-spark/data/person.json")
    personDataFrame.select("name","age")
    personDataFrame.show()
  }

}
