package com.gz.javastudy.mybatisapp.interceptor;

import java.lang.annotation.*;

/**
 * 数据权限自定义注解
 */
@Inherited
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface DataScope {

    // 权限的类型
    DataScopeType type() default DataScopeType.ACT;

    // 限制的字段
    String column() default "";

}