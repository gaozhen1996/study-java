package com.gz.javastudy.mybatisapp.interceptor;

/**
 * @author gaozhen
 */

public enum DataScopeType {

    CLT("客户号类型"),
    ACT("账号类型");

    /**
     *   描述
     */
    private String desc;

    /**
     *   构造方法
     */
    private DataScopeType(String desc) {
        this.desc = desc;
    }

    /**
     * 一般方法
     */
    public String getDesc() {
        return this.desc;
    }
}
