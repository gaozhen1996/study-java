package com.gz.javastudy.mybatisapp.interceptor;

import com.gz.javastudy.mybatisapp.toolkit.PluginUtils;
import org.apache.ibatis.executor.statement.StatementHandler;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.plugin.*;
import org.apache.ibatis.reflection.MetaObject;
import org.apache.ibatis.reflection.SystemMetaObject;

import java.lang.reflect.Method;
import java.sql.Connection;
import java.util.Properties;

@Intercepts(
        {@Signature(
                type = StatementHandler.class,
                method = "prepare",
                args = {Connection.class, Integer.class}
        )})
public class DataScopeInterceptor implements Interceptor {
    @Override
    public Object intercept(Invocation invocation) throws Throwable {
        // 1.获取执行的RoutingStatementHandler
        StatementHandler statementHandler = PluginUtils.realTarget(invocation.getTarget());
        // 2.获取MetaObject
        MetaObject metaObject = SystemMetaObject.forObject(statementHandler);
        // 3.获取MappedStatement, 用于判定方法上的注解
        MappedStatement ms = (MappedStatement) metaObject.getValue("delegate.mappedStatement");
        // 4.MappedStatement的ID即为Mapper方法的全路径名
        String methodId = ms.getId();
        // 5.获取Mapper的Class名称
        String clazzName = methodId.substring(0, methodId.lastIndexOf("."));
        // 6.获取拦截的方法名
        String methodName = methodId.substring(methodId.lastIndexOf(".") + 1);
        // 7.反射获取方法上的注解内容
        Method[] methods = Class.forName(clazzName).getDeclaredMethods();
        DataScope dataScope = null;
        for (Method md : methods) {
            if (methodName.equalsIgnoreCase(md.getName())) {
                dataScope = md.getAnnotation(DataScope.class);
            }
        }
        if (dataScope == null) {
            return invocation.proceed();
        }

        DataScopeType type = dataScope.type();
        String column = dataScope.column();
        if (type==null ||column==null) {
            return invocation.proceed();
        }
        // 8.获取原始执行的SQL
        String sql = (String) metaObject.getValue("delegate.boundSql.sql");
        sql = sql.replaceAll("\\n", "").replaceAll("\\t", "");
        // 9.根据注解内容修改SQL后执行
        if (DataScopeType.CLT.equals(type)) {
            String newSql = "SELECT * FROM (" + sql + ") t1 WHERE t1." + column + " in ('男')";
            metaObject.setValue("delegate.boundSql.sql", newSql);
        }else if (DataScopeType.ACT.equals(type)){
            String newSql = "SELECT * FROM (" + sql + ") t1 WHERE t1." + column + " in ('女')";
            metaObject.setValue("delegate.boundSql.sql", newSql);
        }

        return invocation.proceed();
    }

    @Override
    public Object plugin(Object o) {
        return Plugin.wrap(o,this);
    }

    @Override
    public void setProperties(Properties properties) {

    }


}
