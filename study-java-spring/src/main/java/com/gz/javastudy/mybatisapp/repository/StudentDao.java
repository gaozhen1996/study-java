package com.gz.javastudy.mybatisapp.repository;

import com.gz.javastudy.mybatisapp.interceptor.DataScope;
import com.gz.javastudy.mybatisapp.interceptor.DataScopeType;
import org.apache.ibatis.annotations.Mapper;

import com.gz.javastudy.mybatisapp.domain.Student;

@Mapper
public interface StudentDao {
	@DataScope(type = DataScopeType.ACT,column = "sex")
	public Student selectStudent(String name);
}
