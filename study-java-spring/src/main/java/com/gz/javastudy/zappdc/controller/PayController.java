package com.gz.javastudy.zappdc.controller;

import com.alibaba.fastjson.JSONObject;
import com.gz.javastudy.springboot.SpringUtil;
import com.gz.javastudy.zappdc.domain.Account;
import com.gz.javastudy.zappdc.netty.MessageService;
import com.gz.javastudy.zappdc.repository.AccountMapper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


import java.util.HashMap;

/**
 * @author gaozhen
 */
@Controller
public class PayController {

	@ResponseBody
	@RequestMapping("/npay")
	public HashMap<String, Object> npay(@RequestParam String account,@RequestParam double balance) {
		HashMap<String, Object> returnData = new HashMap<String, Object>(8);
		MessageService messageService = SpringUtil.getBean(MessageService.class);
		JSONObject msg = new JSONObject();
		msg.put("account",account);
		msg.put("balance",balance);
		messageService.sendMessage("balance_update",msg);
		returnData.put("code",500);
		return returnData;
	}

	@ResponseBody
	@RequestMapping("/bpay")
	public HashMap<String, Object> bpay(@RequestParam String account,@RequestParam double balance) {
		HashMap<String, Object> returnData = new HashMap<String, Object>(8);
		Account accountEntity = new Account();
		accountEntity.setAccount(account);
		accountEntity.setBalance(balance);
		AccountMapper accountMapper = SpringUtil.getBean(AccountMapper.class);
		int effect = accountMapper.updateBalanceByAccount(accountEntity);
		if (effect == 1) {
			returnData.put("data",true);
		}else {
		    returnData.put("data", false);
		}
		returnData.put("code",500);
		return returnData;
	}

	@ResponseBody
	@RequestMapping("/npays")
	public HashMap<String, Object> npays(@RequestParam String account,@RequestParam double balance) {
		HashMap<String, Object> returnData = new HashMap<String, Object>(8);
		int index = 100001;
		int max = Integer.parseInt(account);
		long startTime = System.currentTimeMillis();
		MessageService messageService = SpringUtil.getBean(MessageService.class);
		for (int i = index; i <=  max; i++) {
			JSONObject msg = new JSONObject();
			msg.put("account",String.valueOf(i));
			msg.put("balance",balance);
			messageService.sendMessage("balance_update",msg);
		}
		long endTime = System.currentTimeMillis();
		returnData.put("code",500);
		long cost = (endTime - startTime);
		returnData.put("cost",cost+"ms");
		returnData.put("tps",(max-index)*1000/cost);
		return returnData;
	}

	@ResponseBody
	@RequestMapping("/bpays")
	public HashMap<String, Object> bpays(@RequestParam String account,@RequestParam double balance) {
		HashMap<String, Object> returnData = new HashMap<String, Object>(8);
		int index = 100001;
		int max = Integer.parseInt(account);
		StringBuffer fail = new StringBuffer();
		long startTime = System.currentTimeMillis();
		AccountMapper accountMapper = SpringUtil.getBean(AccountMapper.class);
		for (int i = index; i <=  max; i++) {
			Account accountEntity = new Account();
			accountEntity.setAccount(String.valueOf(i));
			accountEntity.setBalance(balance);
			int effect = accountMapper.updateBalanceByAccount(accountEntity);
			if (effect != 1) {
				fail.append(index+",");
			}
		}
		long endTime = System.currentTimeMillis();
		returnData.put("code",500);
		long cost = (endTime - startTime);
		returnData.put("cost",cost);
		returnData.put("tps",(max-index)*1000/cost);
		returnData.put("msg",fail.toString());
		return returnData;
	}
}
