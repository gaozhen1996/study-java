package com.gz.javastudy.zappdc.controller;


import com.alibaba.fastjson.JSONObject;
import com.gz.javastudy.springboot.SpringUtil;
import com.gz.javastudy.zappdc.domain.BusinessFlow;
import com.gz.javastudy.zappdc.netty.MessageService;
import com.gz.javastudy.zappdc.repository.BusinessFlowMapper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;

/**
 * @author gaozhen
 */
@Controller
public class FlowDealController {

    @ResponseBody
    @RequestMapping("/dealFlow")
    public HashMap<String, Object> dealFlow() {
        HashMap<String, Object> returnData = new HashMap<String, Object>(8);
        long startTime = System.currentTimeMillis();
        /**
         * 获取未处理的流水，发送到mock
         */
        BusinessFlowMapper flowMapper = SpringUtil.getBean(BusinessFlowMapper.class);
        List<BusinessFlow> busFlows = flowMapper.selectBusinessFlowByStatus("Pending");
        MessageService messageService = SpringUtil.getBean(MessageService.class);
        for (BusinessFlow flow : busFlows) {
            messageService.sendMessage("business_flow",(JSONObject) JSONObject.toJSON(flow));
        }
        long endTime = System.currentTimeMillis();
        returnData.put("code",500);
        long cost = (endTime - startTime);
        returnData.put("cost",cost+"ms");
        returnData.put("tps",busFlows.size()*1000/cost);
        return returnData;
    }
}
