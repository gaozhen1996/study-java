package com.gz.javastudy.zappdc.repository;

import com.gz.javastudy.zappdc.domain.Account;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @author gaozhen
 */
@Mapper
public interface AccountMapper {
    /**
     * 根据账号更新余额
     * @param account
     * @return
     */
    int updateBalanceByAccount(Account account);

    /**
     * 根据账号获取余额
     * @param account
     * @return
     */
    Account selectBalanceByAccount(@Param("account")String account);
}
