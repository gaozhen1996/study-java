package com.gz.javastudy.zappdc.repository;

import com.gz.javastudy.zappdc.domain.BusinessFlow;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author gaozhen
 */
@Mapper
public interface BusinessFlowMapper {

    /**
     * 根据状态获取业务流水
     * @param status
     * @return
     */
    List<BusinessFlow> selectBusinessFlowByStatus(@Param("status") String status);

    /**
     * 修改业务流水
     * @param businessFlow
     * @return
     */
    int updateBusinessFlowById(BusinessFlow businessFlow);
}
