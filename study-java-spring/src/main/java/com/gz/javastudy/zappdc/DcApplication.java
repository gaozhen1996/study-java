package com.gz.javastudy.zappdc;

import com.gz.javastudy.springboot.SpringApplication;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * @author gaozhen
 */
@Configuration
@EnableWebMvc
@ComponentScan("com.gz.javastudy.zappdc")
@MapperScan(basePackages ="com.gz.javastudy.zappdc.repository")
public class DcApplication {
    public static void main(String[] args) throws InterruptedException {
        SpringApplication.run(DcApplication.class);

    }
}
