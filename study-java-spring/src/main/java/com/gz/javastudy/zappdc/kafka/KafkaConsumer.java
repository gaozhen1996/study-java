package com.gz.javastudy.zappdc.kafka;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.gz.javastudy.springboot.SpringUtil;
import com.gz.javastudy.zappdc.domain.Account;
import com.gz.javastudy.zappdc.domain.BusinessFlow;
import com.gz.javastudy.zappdc.repository.AccountMapper;
import com.gz.javastudy.zappdc.repository.BusinessFlowMapper;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author gaozhen
 */
@Component
public class KafkaConsumer implements ApplicationListener<ContextRefreshedEvent> {

    /**
     * 核心线程数
     */
    private  int corePoolSize = 4;
    /**
     * 最大线程数
     */
    private int maximumPoolSize = 8;
    /**
     * 超过 corePoolSize 线程数量的线程最大空闲时间
     */
    private long keepAliveTime = 2;
    /**
     * 以秒为时间单位
     */
    private TimeUnit unit = TimeUnit.SECONDS;
    /**
     * 创建工作队列，用于存放提交的等待执行任务
     */
    private BlockingQueue<Runnable> workQueue = new ArrayBlockingQueue<Runnable>(2);

    private ThreadPoolExecutor threadPoolExecutor = null;

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(KafkaConsumer.class);

    private ThreadPoolExecutor createThreadPool(){
        this.threadPoolExecutor=new ThreadPoolExecutor(corePoolSize,maximumPoolSize,
                keepAliveTime,unit,workQueue,new ThreadPoolExecutor.CallerRunsPolicy());
        return this.threadPoolExecutor;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        createThreadPool();
        this.threadPoolExecutor.submit(() -> {
            log.info("kafka消费者启动");
            Map<String, Object> kafkaConsumerProperties = (Map<String, Object>) SpringUtil.getBean("kafkaConsumerProperties");
            org.apache.kafka.clients.consumer.KafkaConsumer<String, String> kafkaConsumer = new org.apache.kafka.clients.consumer.KafkaConsumer<String,String>(kafkaConsumerProperties);
            kafkaConsumer.subscribe(Collections.singletonList("topic_test"));
            while (true) {
                ConsumerRecords<String, String> consumerRecords = kafkaConsumer.poll(1000);
                for (ConsumerRecord<String, String> consumerRecord : consumerRecords) {
                    MyTask myTask = new MyTask();
                    myTask.consumerRecord=consumerRecord;
                    this.threadPoolExecutor.execute(myTask);
                }
            }
        });
    }

    static class MyTask implements Runnable{
        ConsumerRecord<String, String> consumerRecord;
        @Override
        public void run() {
            String key = consumerRecord.key();
            switch (key) {
                case "balance_update":balanceUpdate(consumerRecord);break;
                case "business_flow":businessFlow(consumerRecord);break;
                default : log.info(key+"类型没有定义");
            }
        }

        private void businessFlow(ConsumerRecord<String, String> consumerRecord){
            JSONObject value = JSON.parseObject(consumerRecord.value());
            BusinessFlow businessFlow = JSONObject.parseObject(value.toString(), BusinessFlow.class);
            AccountMapper accountMapper = SpringUtil.getBean(AccountMapper.class);
            BusinessFlowMapper flowMapper = SpringUtil.getBean(BusinessFlowMapper.class);
            synchronized (MyTask.class){
                //1.获取余额
                Account account = accountMapper.selectBalanceByAccount(businessFlow.getAccount());
                account.setBalance(account.getBalance()+businessFlow.getAmount());
                //2.更新余额
                int effect = accountMapper.updateBalanceByAccount(account);
                //3.更新流水状态
                if (effect != 1) {
                    log.info(String.format("account:%s更新余额失败",account.getAccount()));
                    businessFlow.setStatus("fail");
                }else {
                    businessFlow.setStatus("success");
                }
                if (flowMapper.updateBusinessFlowById(businessFlow)!=1){
                    log.info(String.format("id:%s更新状态失败",account.getAccount()));
                }
            }

        }

        private void balanceUpdate(ConsumerRecord<String, String> consumerRecord){
            JSONObject value = JSON.parseObject(consumerRecord.value());
            AccountMapper accountMapper = SpringUtil.getBean(AccountMapper.class);
            Account account = JSONObject.parseObject(value.toString(), Account.class);
            int effect = accountMapper.updateBalanceByAccount(account);
            if (effect != 1) {
                log.info(String.format("account:%s更新余额失败",account.getAccount()));
            }
        }
    }
}
