package com.gz.javastudy.springapp.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ConfigReader {
	public static final String LOCATION = System.getProperty("user.dir") + File.separator + "src/main/resources/application.properties";

	// 配置信息
	private Properties properties = new Properties();
	
	public ConfigReader() {
		InputStream in = null;
		try {
			in = new FileInputStream(LOCATION);
			properties.load(in);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (in != null){
					in.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public String getconfigValueByKey(String key) {
		return properties.getProperty(key);
	}
}
