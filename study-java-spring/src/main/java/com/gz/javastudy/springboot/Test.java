package com.gz.javastudy.springboot;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@ComponentScan("com.gz.javastudy.springboot")
@EnableWebMvc
public class Test {
	public static void main(String[] args) {
		SpringApplication.run(Test.class);
	}
}
