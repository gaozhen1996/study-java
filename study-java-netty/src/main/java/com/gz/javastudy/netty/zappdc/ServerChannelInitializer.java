package com.gz.javastudy.netty.zappdc;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.LengthFieldPrepender;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;

public class ServerChannelInitializer extends ChannelInitializer<SocketChannel> {


    @Override
    protected void initChannel(SocketChannel SocketChannel) throws Exception {
        SocketChannel.pipeline().addLast(new LengthFieldBasedFrameDecoder(Integer.MAX_VALUE, 0, 8, 0, 8));
        SocketChannel.pipeline().addLast(new StringDecoder());
        SocketChannel.pipeline().addLast(new ServerHandler());
        SocketChannel.pipeline().addLast(new LengthFieldPrepender(8, false));
        SocketChannel.pipeline().addLast(new StringEncoder());


    }
}
