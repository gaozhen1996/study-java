package com.gz.javastudy.netty.zappdc;

import com.gz.javastudy.netty.rpc.spring.Application;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author gaozhen
 */
public class MockApplication {
    private static volatile boolean running = true;

    @Autowired
    private KafkaProducer producer;

    public static void main(String[] args) {
        try {
            //此处扫描包名，不能包含代理类的包名
            //只需要包含service，监听器的包名即可
            AnnotationConfigApplicationContext context =
                    new AnnotationConfigApplicationContext("com.gz.javastudy.netty.zappdc");
            SpringUtil.setApplicationContext(context);
            //在jvm中增加一个关闭的钩子,当jvm关闭的时候，
            //会执行系统中已经设置的所有通过方法addShutdownHook
            //添加的钩子，当系统执行完这些钩子后，jvm才会关闭。
            Runtime.getRuntime().addShutdownHook(new Thread(() -> {
                try {
                    context.stop();
                } catch (Throwable t) {
                    t.printStackTrace();
                }
                synchronized (Application.class) {
                    running = false;
                    Application.class.notify();
                }
            }));
            context.start();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);

        }
        System.out.println("服务器已启动====");
        synchronized (Application.class) {
            while (running) {
                try {
                    Application.class.wait();
                } catch (Throwable e) {
                }
            }
        }


    }
}
