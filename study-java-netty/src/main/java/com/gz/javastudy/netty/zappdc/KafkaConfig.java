package com.gz.javastudy.netty.zappdc;

import java.util.HashMap;
import java.util.Map;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource(value = {"classpath:kafka.properties"})
public class KafkaConfig {

    /**
     * ----------------------------------------------
     *                   producer
     * ---------------------------------------------
     */
    @Value("${kafka.producer.bootstrap.servers}")
    private String producerBootstrapServer;

    @Value("${kafka.producer.buffer.memory}")
    private Long producerMemory;

    @Value("${kafka.producer.retries}")
    private int producerRetries;

    @Value("${kafka.producer.batch.size}")
    private int producerSize;

    @Value("${kafka.producer.linger.ms}")
    private int producerMs;

    /**
     * ----------------------------------------------
     *                   consumer
     * ---------------------------------------------
     */
    @Value("${kafka.consumer.bootstrap.servers}")
    private String consumerBootstrapServer;

    @Value("${kafka.consumer.group.id}")
    private String consumerGroupID;

    @Value("${kafka.consumer.enable.auto.commit}")
    private Boolean consumerAutoCommit;

    @Value("${kafka.consumer.session.timeout.ms}")
    private Integer consumerSessionTimeout;

    @Bean
    public ProducerFactory<String, String> producerFactory() {
        Map<String, Object> props = new HashMap<>();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, producerBootstrapServer);
        props.put(ProducerConfig.RETRIES_CONFIG, producerRetries);
        props.put(ProducerConfig.BATCH_SIZE_CONFIG, producerSize);
        props.put(ProducerConfig.LINGER_MS_CONFIG, producerMs);
        props.put(ProducerConfig.BUFFER_MEMORY_CONFIG, producerMemory);
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        return new DefaultKafkaProducerFactory<>(props);
    }

    @Bean
    public KafkaTemplate kafkaTemplate() {
        return new KafkaTemplate<>(producerFactory());
    }


    @Bean("kafkaConsumerProperties")
    public Map<String, Object> consumerProperties() {
        Map<String, Object> props = new HashMap<>();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, consumerBootstrapServer);
        props.put(ConsumerConfig.GROUP_ID_CONFIG, consumerGroupID);
        props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, consumerAutoCommit);
        props.put(ConsumerConfig.SESSION_TIMEOUT_MS_CONFIG, 15000);
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        return props;
    }


}
