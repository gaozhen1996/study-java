/*
 * Copyright 2012 The Netty Project
 *
 * The Netty Project licenses this file to you under the Apache License,
 * version 2.0 (the "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package com.gz.javastudy.netty.cases.粘包;

import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.PooledByteBufAllocator;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.LengthFieldPrepender;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;

import java.nio.charset.Charset;


public final class LoadRunnerClient {

    static final String HOST = System.getProperty("host", "127.0.0.1");
    static final int PORT = Integer.parseInt(System.getProperty("port", "18085"));
    public static ChannelFuture future;

    public static void main(String[] args) throws Exception {
        EventLoopGroup group = new NioEventLoopGroup();
        try {
            Bootstrap b = new Bootstrap();
            b.channel(NioSocketChannel.class);
            b.group(group);
            b.option(ChannelOption.ALLOCATOR, PooledByteBufAllocator.DEFAULT);
            b.handler(new ChannelInitializer<SocketChannel>() {
                 @Override
                 public void initChannel(SocketChannel ch) {
                     ch.pipeline().addLast(new LengthFieldBasedFrameDecoder(Integer.MAX_VALUE, 0, 4, 0, 4));
                     //把接收到的Bytebuf数据包转换成String
                     ch.pipeline().addLast(new StringDecoder());
                     //业务逻辑处理handler
                     ch.pipeline().addLast(new LoadRunnerClientHandler());
                     //在消息体前面新增4个字节的长度值,第一个参数长度值字节数大小，
                     //第二个参数长度值是否要包含长度值本身大小
                     ch.pipeline().addLast(new LengthFieldPrepender(4, false));
                     //把字符串消息转换成ByteBuf
                     ch.pipeline().addLast(new StringEncoder(Charset.forName("utf-8")));

                 }
             });
            future = b.connect(HOST, PORT).sync();
            testRpc();
        } finally {
            group.shutdownGracefully();
        }


    }


    private static void testRpc() throws Exception {
        for(int i=0;i<1000;i++) {
            System.out.println("send f");
            future.channel().writeAndFlush("{abc}");
            Thread.sleep(1);
        }
    }
}
