import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.RangeQueryBuilder;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.util.StringUtils;
import com.gz.javastudy.es.domain.Product;

import javax.annotation.Resource;

@SpringBootTest
public class ElasticsearchRestTemplateQuery {

    @Resource
    private ElasticsearchRestTemplate restTemplate;
    /**
     * 匹配及范围查询
     */
    @Test
    void test5(){
        //查询条件
        String name="男衬衫";
        //查询范围
        Double from=100D;
        Double to=150D;

        //创建rangequery,添加范围参数
        RangeQueryBuilder range = new RangeQueryBuilder("price");
        if (from!=null)
        range.gte(from);
        if (to!=null)
        range.lte(to);

        //创建boolquery,多条件使用bool查询
        BoolQueryBuilder bool = new BoolQueryBuilder();
        //match匹配
        bool.must(new MatchQueryBuilder("name",name));
        bool.must(range);

        //创建query对象
        NativeSearchQuery searchQuery = new NativeSearchQueryBuilder().withQuery(bool).build();

        //查询
        SearchHits<Product> searchHits = restTemplate.search(searchQuery, Product.class);

        for (SearchHit<Product> searchHit : searchHits) {
            System.out.println(searchHit.getContent());
        }

    }

    /**
     * 范围查询
     * 动态
     */
    @Test
    void test4(){
        //定义范围
        Double from=100D;
        Double to=150D;

        //创建rangquery
        RangeQueryBuilder range = new RangeQueryBuilder("price");
        //Double类型只需要判断为null
        if (from!=null)
        range.gte(from);
        if (to!=null)
        range.lte(to);


        //创建query对象
        NativeSearchQuery searchQuery = new NativeSearchQueryBuilder().withQuery(range).build();

        //查询
        SearchHits<Product> searchHits = restTemplate.search(searchQuery, Product.class);

        //遍历
        for (SearchHit<Product> searchHit : searchHits) {
            System.out.println(searchHit.getContent());
        }
    }

    /**
     * 多字段动态查询-查询条件可传可不传
     */
    @Test
    void test3(){
        //查询条件
        String name="男衬衫";
        String merchant=null;

        //创建boolquery
        BoolQueryBuilder bool = new BoolQueryBuilder();
        if (StringUtils.hasText(name))
        bool.must(new MatchQueryBuilder("name",name));
        if (StringUtils.hasText(merchant))
        bool.must(new MatchQueryBuilder("merchant",merchant));

        //创建query对象
        NativeSearchQuery searchQuery = new NativeSearchQueryBuilder().withQuery(bool).build();

        //查询
        SearchHits<Product> searchHits = restTemplate.search(searchQuery, Product.class);
        for (SearchHit<Product> searchHit : searchHits) {
            System.out.println(searchHit.getContent());
        }

    }

 /**
     * 查询多字段-boolquery
     */
    @Test
    void test1(){
        //查询字段
        String name="男衬衫";
        String merchant="优衣库";


        //创建boolquery对象
        BoolQueryBuilder bool = new BoolQueryBuilder();
        bool.must(new MatchQueryBuilder("name",name));
        bool.must(new MatchQueryBuilder("merchant",merchant));



        //创建query对象
        NativeSearchQuery searchQuery = new NativeSearchQueryBuilder().withQuery(bool).build();


        //执行查询
        SearchHits<Product> searchHits = restTemplate.search(searchQuery, Product.class);

        for (SearchHit<Product> searchHit : searchHits) {
            System.out.println(searchHit);
            Product content = searchHit.getContent();
            System.out.println("content = " + content);
        }
    }

    /**
     * 单字段查询-matchquery
     */
    @Test
    void test2(){
        String name="男衬衫";

        //创建matchquery
        MatchQueryBuilder match = new MatchQueryBuilder("name",name);


        //构建query对象
        NativeSearchQuery searchQuery = new NativeSearchQueryBuilder().withQuery(match).build();

        SearchHits<Product> searchHits = restTemplate.search(searchQuery, Product.class);

        for (SearchHit<Product> searchHit : searchHits) {
            //打印商品数据
            System.out.println(searchHit.getContent());
        }
    }
}
