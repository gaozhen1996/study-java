import com.gz.javastudy.es.EsApplication;
import com.gz.javastudy.es.repository.ProductRepository;
import com.gz.javastudy.es.domain.Product;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * elasticSearch的repository
 * 已提供的接口,可以直接查询数据
 * 增删改查
 */
@SpringBootTest(classes = EsApplication.class)
public class MyTest {
    
    @Resource
    private ProductRepository productRepository;

    /**
     * 查询所有-按指定字段排序-分页显示
     */
    @Test
    void test9(){
        int page=1;
        int size=5;
        //排序
        Sort price = Sort.by(Sort.Order.asc("price"));
        PageRequest pageRequest = PageRequest.of(page, size, price);

        Page<Product> products = productRepository.findAll(pageRequest);

        for (Product product : products) {
            System.out.println(product);
        }

    }

    /**
     * 排序查询-sort
     */
    @Test
    void test8(){
        //填入参数-根据es中索引的属性来排序
        Sort price = Sort.by(Sort.Order.asc("price"));
        //查询-排序
        Iterable<Product> products = productRepository.findAll(price);

        for (Product product : products) {
            System.out.println(product);
        }
    }


    /**
     * 分页查询所有商品
     */
    @Test
    void test7(){
        //当前页,从0开始
        int page=1;//实际页码为2,需减1
        //每页显示行数
        int size=5;
        PageRequest pageRequest =  PageRequest.of(page-1, size);
        Page<Product> products = productRepository.findAll(pageRequest);

        //遍历
        for (Product product : products) {
            System.out.println( product);
        }
    }

    /**
     * 根据问的文档id查询
     */
    @Test
    void test6(){
        Optional<Product> optional = productRepository.findById(1L);
        System.out.println("optional = " + optional);
        Product product = optional.get();
        System.out.println("product = " + product);

        Iterable<Product>  products= productRepository.findAll();
        System.out.println("products = " + products);


    }

    /**
     * 查询总数
     */
    @Test
    void test5(){
        long count = productRepository.count();
        System.out.println("count = " + count);
    }

    /**
     * 删除-根据对象
     */
    @Test
    void test4(){
        Optional<Product> optional = productRepository.findById(100L);
        System.out.println("optional = " + optional);
        Product product = optional.get();
        System.out.println("product = " + product);
        productRepository.delete(product);
        System.out.println("删除成功");
    }

    /**
     * 删除-根据id
     */
    @Test
    void test3(){
        productRepository.deleteById(100L);
        System.out.println("删除成功..");
    }


    /**
     * 添加/修改
     * id原来没有-添加,
     * id有-修改,先删除再添加
     */
    @Test
    void test2(){
        Product product1 = new Product(101l, "毛衣", "华夏衣品旗舰店", 110.0, "深圳", "广东");
        Product product2 = new Product(102l, "长裤", "361旗舰店", 110.0, "义乌", "浙江");
        List<Product> products = new ArrayList<>();
        products.add(product1);
        products.add(product2);
        //根据id添加或修改
        productRepository.saveAll(products);
    }

    /**
     * 查询所有商品-product
     */
    @Test
    void test1(){
        Iterable<Product> products = productRepository.findAll();

        for (Product product : products) {
            System.out.println(product);
        }
    }

    /**
     * 自定义查询-使用es的查询方式
     */
    @Test
    void findByName1() {
        List<Product> products = productRepository.findByName1();
        for (Product product : products) {
            System.out.println(product);
        }

    }
    
}
