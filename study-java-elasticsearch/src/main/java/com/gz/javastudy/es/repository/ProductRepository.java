package com.gz.javastudy.es.repository;

import com.gz.javastudy.es.domain.Product;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.annotations.Highlight;
import org.springframework.data.elasticsearch.annotations.HighlightField;
import org.springframework.data.elasticsearch.annotations.HighlightParameters;
import org.springframework.data.elasticsearch.annotations.Query;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.List;

/**
 * @author gaozhen
 */
public interface ProductRepository extends ElasticsearchRepository<Product,Long> {

    /**
     * 通过name查找
     * @param name
     * @return
     */
    List<Product> findByName(String name);

    /**
     * 通过name查找，注解方式
     * @return
     */
    @Query("{\"match\":{\"name\":\"男衬衫\"}}")
    List<Product> findByName1();

    List<Product> findByNameAndMerchant(String name, String merchant);

    @Query("{\"bool\":{\"must\":[{\"match\":{\"name\":\"男衬衫\"}},{\"match\":{\"merchant\":\"旗舰店\"}}]}}")
    List<Product> findByNameAndMerchant2(String name, String merchant);

    List<Product> findByNameAndPriceBetween(String name, double low, double up);

    @Query("{\"bool\":{\"must\":[{\"match\":{\"name\":\"男衬衫\"}},{\"range\":{\"price\":{\"gte\":50,\"lte\":200}}}]}}")
    List<Product> findByNameAndPriceBetween2(String name, double low, double up);

    List<Product> findByNameOrderByPriceAsc(String name);

    List<Product> findByNameAndMerchantAndPriceBetweenOrderByPriceAsc(String name, String merchant, Double from, Double to);

    List<Product> findByMerchant(String merchant, PageRequest pageRequest);

    @Highlight(fields = {
            @HighlightField(name = "name"),
            @HighlightField(name="city")
    },
            parameters = @HighlightParameters(
                    preTags = "<font color=red>",
                    postTags = "</font>"
            )
    )
    List<SearchHit<Product>> findByNameAndCity(String name, String city);
}
