package com.gz.javastudy.es.service;





import com.gz.javastudy.es.common.PageResult;
import com.gz.javastudy.es.domain.Answer;
import com.gz.javastudy.es.domain.Product;

import java.util.List;
import java.util.Map;

/**
 * @author gaozhen
 */
public interface AnswerService {
    /**
     * 分页查找
     * @param page
     * @param size
     * @param params
     * @return
     */
    PageResult<Answer> findPage(Integer page, Integer size, Map<String, String> params);

    /**
     * 关键词查询
     * @param keyWord
     * @return
     */
    List<Answer> findKeyWord(String keyWord);
}
