package com.gz.javastudy.es.controller;

import com.gz.javastudy.es.common.PageResult;
import com.gz.javastudy.es.common.Result;
import com.gz.javastudy.es.domain.Product;
import com.gz.javastudy.es.service.ProductService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @author gaozhen
 */
@RestController
@RequestMapping("/product")
public class ProductController {

    @Resource
    private ProductService productService;

    /**
     * 商品列表分页条件查询,高亮显示查询结果
     * - 搜索引擎查询
     * page 页码
     * size 大小
     * name 商品名称
     * merchant 商家名称
     * form 起始价格
     * to 结束价格
     * orderField 排序字段
     * orderType 排序方式
     */
    @GetMapping("/findPage/{page}/{size}")
    public Result findPage(@PathVariable Integer page, @PathVariable Integer size,
                           @RequestBody(required = false) Map<String,String> params){

        PageResult<Product> pageResult=productService.findPage(page,size,params);

        return new Result(true,"分页条件查询success",pageResult);
    }

    @GetMapping("/findKeyWord/{keyWord}")
    public Result findPage(@PathVariable String keyWord){
        List<Product> pageResult=productService.findKeyWord(keyWord);
        return new Result(true,"关键词查询",pageResult);
    }
}
