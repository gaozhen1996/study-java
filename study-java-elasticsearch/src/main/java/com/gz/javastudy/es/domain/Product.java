package com.gz.javastudy.es.domain;

import org.springframework.data.elasticsearch.annotations.Document;

@Document(indexName = "product")//指定映射es中的索引
public class Product {
    private Long id;
    private String name;
    private String merchant;
    private Double price;
    private String city;
    private String province;

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", merchant='" + merchant + '\'' +
                ", price=" + price +
                ", city='" + city + '\'' +
                ", province='" + province + '\'' +
                '}';
    }
    public Product(){}
    public Product(Long id, String name, String merchant, Double price, String city, String province) {
        this.id = id;
        this.name = name;
        this.merchant = merchant;
        this.price = price;
        this.city = city;
        this.province = province;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMerchant() {
        return merchant;
    }

    public void setMerchant(String merchant) {
        this.merchant = merchant;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }
}
