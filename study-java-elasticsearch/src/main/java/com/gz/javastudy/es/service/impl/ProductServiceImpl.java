package com.gz.javastudy.es.service.impl;


import com.gz.javastudy.es.common.PageResult;
import com.gz.javastudy.es.domain.Product;
import com.gz.javastudy.es.service.BaseSearchService;
import com.gz.javastudy.es.service.ProductService;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @author gaozhen
 */
@Service
public class ProductServiceImpl extends BaseSearchService<Product> implements ProductService {

    @Resource
    private ElasticsearchRestTemplate restTemplate;

    @Override
    public PageResult<Product> findPage(Integer page, Integer size, Map<String, String> params) {
        return findPage(Product.class,page,size,params);
    }

    @Override
    public List<Product> findKeyWord(String keyWord) {
        List<Product> products = findKeyWord(keyWord, Product.class);
        return products;
    }

}
