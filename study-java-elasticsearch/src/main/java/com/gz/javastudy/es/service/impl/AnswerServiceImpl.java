package com.gz.javastudy.es.service.impl;


import com.gz.javastudy.es.common.PageResult;
import com.gz.javastudy.es.domain.Answer;
import com.gz.javastudy.es.domain.Product;
import com.gz.javastudy.es.service.AnswerService;
import com.gz.javastudy.es.service.BaseSearchService;
import com.gz.javastudy.es.service.ProductService;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @author gaozhen
 */
@Service
public class AnswerServiceImpl extends BaseSearchService<Answer> implements AnswerService {

    @Resource
    private ElasticsearchRestTemplate restTemplate;

    @Override
    public PageResult<Answer> findPage(Integer page, Integer size, Map<String, String> params) {
        return findPage(Answer.class,page,size,params);
    }

    @Override
    public List<Answer> findKeyWord(String keyWord) {
        return findKeyWord(keyWord, Answer.class);
    }

}
