package com.gz.javastudy.es.domain;

import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;

@Document(indexName = "deal_answer")//指定映射es中的索引
public class Answer {

    @Field("question_id")
    private String questionId;

    @Field("question_title")
    private String questionTitle;

    @Field("answer_id")
    private String answerId;

    @Field("answer_content")
    private String answerContent;

    @Field("author_name")
    private String authorName;

    @Field("author_headline")
    private String authorHeadline;

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public String getQuestionTitle() {
        return questionTitle;
    }

    public void setQuestionTitle(String questionTitle) {
        this.questionTitle = questionTitle;
    }

    public String getAnswerId() {
        return answerId;
    }

    public void setAnswerId(String answerId) {
        this.answerId = answerId;
    }

    public String getAnswerContent() {
        return answerContent;
    }

    public void setAnswerContent(String answerContent) {
        this.answerContent = answerContent;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getAuthorHeadline() {
        return authorHeadline;
    }

    public void setAuthorHeadline(String authorHeadline) {
        this.authorHeadline = authorHeadline;
    }

    @Override
    public String toString() {
        return "Answer{" +
                "questionId='" + questionId + '\'' +
                ", questionTitle='" + questionTitle + '\'' +
                ", answerId='" + answerId + '\'' +
                ", answerContent='" + answerContent + '\'' +
                ", authorName='" + authorName + '\'' +
                ", authorHeadline='" + authorHeadline + '\'' +
                '}';
    }
}
