package com.gz.javastudy.es.service;

import com.gz.javastudy.es.common.PageResult;
import org.elasticsearch.index.query.*;
import org.elasticsearch.search.sort.SortBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.util.StringUtils;


import javax.annotation.Resource;
import java.util.*;
/**
 * @author gaozhen
 */
public class BaseSearchService<T> {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Resource
    private ElasticsearchRestTemplate restTemplate;

    public PageResult<T> findPage(Class<T> clazz ,int page, Integer size, Map<String, String> params) {
        //需要判断params是否为空,否则使用该对象的方法时报异常
        if (params==null){
            params=new HashMap<>(8);
        }

        //bool查询
        BoolQueryBuilder boolQueryBuilder = getBoolQuery(params);

        //分页查询
        PageRequest pageRequest = PageRequest.of(page - 1, size);

        //sort排序查询
        //使用默认score排序
        SortBuilder sortBuilder = SortBuilders.scoreSort();
        //判断是否含有排序的参数,有的话则换掉排序方式
        if (StringUtils.hasText(params.get("orderField")) && StringUtils.hasText(params.get("orderType"))){
            //添加排序字段
            sortBuilder=SortBuilders.fieldSort(params.get("orderField"));
            //添加排序方式
            String asc = "asc";
            String desc = "desc";
            String orderType = "orderType";

            if (asc.equals(params.get(orderType))){
                sortBuilder.order(SortOrder.ASC);
            }else if (desc.equals(orderType)){
                sortBuilder.order(SortOrder.DESC);
            }
        }

        //高亮查询
        HighlightBuilder highlightBuilder = createHighlightBuilder();

        //创建query请求
        NativeSearchQuery searchQuery = new NativeSearchQueryBuilder()
                //bool参数查询
                .withQuery(boolQueryBuilder)
                //分页查询携带分页参数
                .withPageable(pageRequest)
                //排序查询携带排序参数
                .withSort(sortBuilder)
                //高亮查询携带参数
                .withHighlightBuilder(highlightBuilder)
                .build();

        //查询
        List<T> list = new ArrayList<>();
        org.springframework.data.elasticsearch.core.SearchHits<T> searchHits = restTemplate.search(searchQuery, clazz);
        for (org.springframework.data.elasticsearch.core.SearchHit<T> searchHit : searchHits) {
            //获取普通结果
            T content = searchHit.getContent();
            list.add(content);
        }
        PageResult<T> pageResult = new PageResult<>();
        pageResult.setTotal((long) list.size());
        pageResult.setRows(list);
        return pageResult;
    }

    public List<T> findKeyWord(String keyword, Class<T> clazz) {

        //1.创建请求
        NativeSearchQuery searchQuery = new NativeSearchQueryBuilder()
                .withQuery(new QueryStringQueryBuilder(keyword))
                .withSort(SortBuilders.scoreSort().order(SortOrder.DESC))
                // .withSort(new FieldSortBuilder("createTime").order(SortOrder.DESC))
                .build();
        //2.查询
        org.springframework.data.elasticsearch.core.SearchHits<T> searchHits = restTemplate.search(searchQuery, clazz);
        
        //3.最终结果集
        List<T> list=new ArrayList<>();
        for (org.springframework.data.elasticsearch.core.SearchHit<T> searchHit : searchHits) {
            //获取普通结果
            T content = searchHit.getContent();
            list.add(content);
        }
        return list;
    }



    /**
     * 构造高亮器
     * @author gaozhen
     */
    private HighlightBuilder createHighlightBuilder(String... fieldNames){
        // 设置高亮,使用默认的highlighter高亮器
        HighlightBuilder highlightBuilder = new HighlightBuilder()
                // .field("productName")
                .preTags("<span style='color:red'>")
                .postTags("</span>");

        // 设置高亮字段
        for (String fieldName: fieldNames) {
            highlightBuilder.field(fieldName);
        }

        return highlightBuilder;
    }


    private List<Map<String,Object>> getHitList(org.springframework.data.elasticsearch.core.SearchHits<T> hits){
        List<Map<String,Object>> list = new ArrayList<>();
        Map<String,Object> map;
        for (org.springframework.data.elasticsearch.core.SearchHit<T> searchHit : hits){
            map = new HashMap<>();
            System.out.println(searchHit);
            searchHit.getHighlightFields().forEach((k,v) -> {
                String height = "";
                List<String> ss = v;
                System.out.println(ss);
            });
        }

        return list;
    }

    /**
     * @author gaozhen
     * @param params
     * @return BoolQueryBuilder
     */
    private BoolQueryBuilder getBoolQuery(Map<String, String> params) {
        BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();

        //范围查询rangeQuery
        RangeQueryBuilder range = new RangeQueryBuilder("price");
        //添加范围参数-if判断动态添加
        if (StringUtils.hasText(params.get("from"))) {
            range.gte(params.get("from"));
        }
        if (StringUtils.hasText(params.get("to"))) {
            range.lte(params.get("to"));
        }

        //条件查询name,merchant
        if (StringUtils.hasText((CharSequence) params.get("name"))) {
            boolQueryBuilder.must(new MatchQueryBuilder("name",params.get("name")));
        }
        if (StringUtils.hasText(params.get("merchant"))) {
            boolQueryBuilder.must(new MatchQueryBuilder("merchant",params.get("merchant")));
        }
        //添加rangeQuery
        boolQueryBuilder.must(range);

        return boolQueryBuilder;
    }
}
