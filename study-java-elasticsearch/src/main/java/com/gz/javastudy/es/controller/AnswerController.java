package com.gz.javastudy.es.controller;

import com.gz.javastudy.es.common.PageResult;
import com.gz.javastudy.es.common.Result;
import com.gz.javastudy.es.domain.Answer;
import com.gz.javastudy.es.domain.Product;
import com.gz.javastudy.es.service.AnswerService;
import com.gz.javastudy.es.service.ProductService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @author gaozhen
 */
@RestController
@RequestMapping("/answer")
public class AnswerController {

    @Resource
    private AnswerService answerService;

    @GetMapping("/findKeyWord/{keyWord}")
    public Result findPage(@PathVariable String keyWord){
        List<Answer> pageResult=answerService.findKeyWord(keyWord);
        return new Result(true,"关键词查询",pageResult);
    }
}
