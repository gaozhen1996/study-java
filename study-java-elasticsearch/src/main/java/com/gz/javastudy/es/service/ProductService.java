package com.gz.javastudy.es.service;





import com.gz.javastudy.es.common.PageResult;
import com.gz.javastudy.es.domain.Product;

import java.util.List;
import java.util.Map;

/**
 * @author gaozhen
 */
public interface ProductService {
    /**
     * 分页查找
     * @param page
     * @param size
     * @param params
     * @return
     */
    PageResult<Product> findPage(Integer page, Integer size, Map<String, String> params);

    /**
     * 关键词查询
     * @param keyWord
     * @return
     */
    List<Product> findKeyWord(String keyWord);
}
