package com.gz.javastudy.concurrent.threadPoolManager;

import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * 线程池管理 管理整个项目中所有的线程，所以不能有多个实例对象
 */
public class ThreadPoolManager {

    private static ThreadPoolManager mInstance = new ThreadPoolManager();

    public static ThreadPoolManager getInstance() {
        return mInstance;
    }


    /**
     * 核心线程池的数量，同时能够执行的线程数量
     */
    private int corePoolSize;

    /**
     * 最大线程池数量，表示当缓冲队列满的时候能继续容纳的等待任务的数量
     */
    private int maximumPoolSize;

    /**
     * 存活时间
     */
    private long keepAliveTime = 3;

    private TimeUnit unit = TimeUnit.MINUTES;
    private ThreadPoolExecutor executor;

    private ThreadPoolManager() {
        /**
         * 给corePoolSize赋值：当前设备可用处理器核心数*2 + 1,能够让cpu的效率得到最大程度执行（有研究论证的）
         */
        corePoolSize = Runtime.getRuntime().availableProcessors() * 2 + 1;
        //虽然maximumPoolSize用不到，但是需要赋值，否则报错
        maximumPoolSize = 300;
        executor = new ThreadPoolExecutor(
                //当某个核心任务执行完毕，会依次从缓冲队列中取出等待任务
                corePoolSize,
                //5,先corePoolSize,然后new LinkedBlockingQueue<Runnable>(),然后maximumPoolSize,但是它的数量是包含了corePoolSize的
                maximumPoolSize,
                //表示的是maximumPoolSize当中等待任务的存活时间
                keepAliveTime,
                unit,
                //缓冲队列，用于存放等待任务，Linked的先进先出
                new LinkedBlockingQueue<Runnable>(),
                //创建线程的工厂
                Executors.defaultThreadFactory(),
                //用来对超出maximumPoolSize的任务的处理策略
                new ThreadPoolExecutor.CallerRunsPolicy()
        );
    }

    /**
     * 执行任务
     */
    public void execute(Runnable runnable) {
        DataSyncThread syncThread = new DataSyncThread(runnable);
        executor.execute(syncThread);
    }

    public class DataSyncThread implements Runnable {
        Runnable runnable;

        public DataSyncThread(Runnable runnable) {
            this.runnable = runnable;
        }

        @Override
        public void run() {
            runnable.run();
        }
    }

    /**
     * 从线程池中移除任务
     */
    public void remove(Runnable runnable) {
        if (runnable == null) {
            return;
        }
        executor.remove(runnable);
    }

    /**
     * 取出执行器
     */
    public ThreadPoolExecutor getExecutor() {
        return executor;
    }

}
