package com.gz.javastudy.concurrent.ticket;

import java.util.ArrayList;

public class SaleTicketByRepeat implements Runnable {

    static int intTickets;
    private String saleWindows;
    static{
        intTickets = 10;
    }

    public SaleTicketByRepeat(String saleWindows, ArrayList<Integer> bugTickets) {
        this.saleWindows = saleWindows;
    }

    @Override
    public void run() {
        while(intTickets>0) {
//            synchronized(SaleTicketByRepeat.class) {
                if(intTickets>0) {
                    System.out.println(saleWindows+"售出票号："+intTickets);
                    intTickets--;
                    try {
                        Thread.sleep(10);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
//            }
        }

    }


    public static void main(String[] args) {
        ArrayList<Integer> bugTickets1 = new ArrayList<>();
        Thread thread1 = new Thread(new SaleTicketByRepeat("窗口1",bugTickets1));
        ArrayList<Integer> bugTickets2 = new ArrayList<>();
        Thread thread2 = new Thread(new SaleTicketByRepeat("窗口2",bugTickets2));
        ArrayList<Integer> bugTickets3 = new ArrayList<>();
        Thread thread3 = new Thread(new SaleTicketByRepeat("窗口3",bugTickets3));
        thread1.start();
        thread2.start();
        thread3.start();
    }

}
