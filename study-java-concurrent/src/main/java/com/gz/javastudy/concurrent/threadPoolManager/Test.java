package com.gz.javastudy.concurrent.threadPoolManager;


import com.google.common.collect.Lists;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

public class Test {
    public static void main(String[] args) {
        Test test = new Test();
        List<Integer> list = new ArrayList<>();
        for(int i=1;i<=100;i++){
            list.add(i);
        }
        List<List<Integer>> partition = Lists.partition(list, 10);
        CountDownLatch countDownLatch = new CountDownLatch(partition.size());
        partition.forEach(s->{
            ThreadPoolManager.getInstance().execute(()->test.mutilCal(s,countDownLatch));
        });
        try {
            countDownLatch.await();
        }catch (Exception e){
            e.printStackTrace();
        }
        System.exit(1);
    }

    public void mutilCal(List<Integer> list, CountDownLatch countDownLatch){
        for (Integer i:list){
            System.out.println(Thread.currentThread()+":"+i);
            try{
                Thread.sleep(2000);
            }catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        countDownLatch.countDown();
    }
}
