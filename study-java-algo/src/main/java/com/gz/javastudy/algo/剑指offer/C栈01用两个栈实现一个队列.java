package com.gz.javastudy.algo.剑指offer;

import java.util.Stack;

/**
 * 用两个栈实现一个队列。队列的声明如下，请实现它的两个函数 appendTail 和 deleteHead ，分别完成在队列尾部插入整数和在队列头部删除整数的功能。(若队列中没有元素，deleteHead操作返回 -1 )
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/yong-liang-ge-zhan-shi-xian-dui-lie-lcof
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */
public class C栈01用两个栈实现一个队列 {

    private Stack<Integer> s1 = new Stack<>();
    private Stack<Integer> s2 = new Stack<>();

    public static void main(String[] args) {
        C栈01用两个栈实现一个队列 test = new C栈01用两个栈实现一个队列();
        test.appendTail(1);
        test.appendTail(2);
        test.appendTail(3);

        test.deleteHead();
        test.deleteHead();

        System.out.println(test.s1);
    }

    public void appendTail(int value) {
        s1.push(value);
    }

    public int deleteHead() {
        System.out.println(s1);
        while (s1.size() > 0){
            s2.push(s1.pop());
        }

        int result = s2.pop();
        while (s2.size() > 0){
            s1.push(s2.pop());
        }
        return result;
    }
}
