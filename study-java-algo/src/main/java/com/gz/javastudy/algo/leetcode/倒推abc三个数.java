package com.gz.javastudy.algo.leetcode;

import java.util.Arrays;

/**
 * @author gaozhen
 * @title: Test
 * @projectName study-java
 * @description: TODO
 * @date 2021/12/30下午8:10
 */
/*
有三个数 a、b、c，大小在 [1,10000000000] 区间内，并且 a<=b<=c 。
现在给定一个数组，数组中包含 7 个数，分别是 a,b,c,a+b,a+c,b+c,a+b+c ，并将其打乱顺序排列。
请根据这 7 个数，倒推出 a、b、c 三个数。
样例1:
[输入]
1 3 4 4 5 7 8
[输出]
1 3 4
[说明]
 */
public class 倒推abc三个数 {

    public static void main(String[] args) {
        int[] arr = {2,3,10,5,12,13,15};
//        int[] arr = {1 , 3 ,  4 , 4 , 5 , 7 , 8};
        Arrays.sort(arr);
        int c = arr[6] - arr[0] - arr[1];
        for (int i=0;i<2;i++){
            System.out.print(arr[i]+" ");
        }
        System.out.print(c+" ");

    }

}
