package com.gz.javastudy.algo.剑指offer;

/**
 * 地上有一个m行n列的方格，从坐标 [0,0] 到坐标 [m-1,n-1] 。一个机器人从坐标 [0, 0] 的格子开始移动，它每次可以向左、右、上、下移动一格（不能移动到方格外），也不能进入行坐标和列坐标的数位之和大于k的格子。例如，当k为18时，机器人能够进入方格 [35, 37] ，因为3+5+3+7=18。但它不能进入方格 [35, 38]，因为3+5+3+8=19。请问该机器人能够到达多少个格子？
 *
 *
 * 示例 1：
 *
 示例1
 输入：
 1,2,3
 复制
 返回值：
 3
 复制
 示例2
 输入：
 0,1,3
 复制
 返回值：
 1
 复制
 示例3
 输入：
 10,1,100
 复制
 返回值：
 29
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/ji-qi-ren-de-yun-dong-fan-wei-lcof
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */
public class F回溯02机器人的运动范围 {

    private int result;

    public static void main(String[] args) {
        F回溯02机器人的运动范围 test = new F回溯02机器人的运动范围();
        System.out.println(test.movingCount(10,1,100));
    }

    public int movingCount(int threshold, int rows, int cols) {
        int[][] marks = new int[rows][cols];
        dfs(0,0,rows,cols,marks,threshold);
        return result;
    }

    public void dfs(int x,int y,int rows,int cols,int[][] mark,int threshold){
        // 第一步，检查下标
        if (x >= rows || y >= cols){
            return;
        }
        // 第二步：检查是否被访问过，或者是否满足当前匹配条件
        if(mark[x][y]==1 || sum(x)+sum(y)>threshold){
            return;
        }
        // 第三步：代码走到这里，说明当前坐标符合条件
        mark[x][y]=1;
        result++;
        // 第四步：都没有返回，说明应该进行下一步递归
        dfs(x+1,y,rows,cols,mark,threshold);
        dfs(x,y+1,rows,cols,mark,threshold);
    }

    public int sum(int num){
        int sum = 0;
        while (num >= 10){
            sum += num%10;
            num /= 10;
        }
        sum += num;
        return sum;
    }


}
