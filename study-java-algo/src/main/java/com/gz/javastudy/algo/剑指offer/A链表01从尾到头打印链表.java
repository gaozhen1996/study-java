package com.gz.javastudy.algo.剑指offer;

import java.util.ArrayList;

/**
 * 描述
 * 输入一个链表的头节点，按链表从尾到头的顺序返回每个节点的值（用数组返回）。
 *
 * 0 <= 链表长度 <= 10000
 *
 *示例1
 * 输入：{1,2,3}
 * 返回值：[3,2,1]
 *
 * 示例2
 * 输入：{67,0,24,58}
 * 返回值：[58,24,0,67]
 */
public class A链表01从尾到头打印链表 {

    public static class ListNode {
        int val;
        ListNode next = null;
        ListNode(int val) {
            this.val = val;
        }
    }

    public static void main(String[] args) {
        A链表01从尾到头打印链表 test = new A链表01从尾到头打印链表();
        ListNode n1 = new ListNode(1);
        ListNode n2 = new ListNode(2);
        ListNode n3 = new ListNode(3);
        n1.next = n2;
        n2.next = n3;
        System.out.println(test.printListFromTailToHead(n1));
    }

    public ArrayList<Integer> printListFromTailToHead(ListNode listNode) {
        ArrayList<Integer> list = new ArrayList<>();
        while (listNode!= null) {
            list.add(listNode.val);
            listNode = listNode.next;
        }
        ArrayList<Integer> result = new ArrayList<>(list.size());
        for (int i = list.size() - 1; i >= 0; i--) {
            result.add(list.get(i));
        }
        return result;
    }
}
