package com.gz.javastudy.algo.剑指offer;

/*
题目：一个二维数组，按行按列都是递增的，要求程序在尽可能小的复杂度的情况下查找给定的元素。

例如：a[4][5]={
         {1, 3, 7, 11, 19},
         {2, 7, 10, 29, 30},
         {13, 28, 54, 69, 90},
         {46, 57, 78, 98, 101}
     }查找29，返回其下标（1,3）。
 */
public class D搜索02二维递增数组的快速查找 {

    public static void getPos(int[][] matrix,int target){
       int col = matrix[0].length;
       int row = matrix.length;
       for(int i=0,j=col-1;i<row && j>=0;){
            if(matrix[i][j]==target){
                System.out.println(i+","+j);
                return;
            }else if(matrix[i][j]<target){
                i++;
            }else {
                j--;
            }
       }

    }
    public static void main(String[] args) {
//        int[][] a=
//                {
//                {1, 3, 7, 11, 19},
//                {2, 7, 10, 29, 30},
//                {13, 28, 54, 69, 90},
//                {46, 57, 78, 98, 101}
//                };
        int[][] a=
                {{5}
                };
        getPos(a,-10);

    }
}
