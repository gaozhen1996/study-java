package com.gz.javastudy.core;

public class JobDef {
    private String name;
    private String cron;
    private int shardTotalCount;
    private String className;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCron() {
        return cron;
    }

    public void setCron(String cron) {
        this.cron = cron;
    }

    public int getShardTotalCount() {
        return shardTotalCount;
    }

    public void setShardTotalCount(int shardTotalCount) {
        this.shardTotalCount = shardTotalCount;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    @Override
    public String toString() {
        return "[" +
                "name:'" + name + '\'' +
                ", cron:'" + cron + '\'' +
                ", shardTotalCount:" + shardTotalCount +
                ", className:'" + className + '\'' +
                ']';
    }
}
