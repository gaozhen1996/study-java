package com.gz.javastudy;

import com.dangdang.ddframe.job.api.simple.SimpleJob;
import com.dangdang.ddframe.job.config.JobCoreConfiguration;
import com.dangdang.ddframe.job.config.simple.SimpleJobConfiguration;
import com.dangdang.ddframe.job.lite.api.JobScheduler;
import com.dangdang.ddframe.job.lite.config.LiteJobConfiguration;
import com.dangdang.ddframe.job.lite.internal.schedule.JobRegistry;
import com.dangdang.ddframe.job.reg.base.CoordinatorRegistryCenter;
import com.dangdang.ddframe.job.reg.zookeeper.ZookeeperConfiguration;
import com.dangdang.ddframe.job.reg.zookeeper.ZookeeperRegistryCenter;

public class ElasticJobHandler {

    private CoordinatorRegistryCenter registryCenter;


    public ElasticJobHandler(){
        //配置分布式Zookeeper分布式协调中心
        ZookeeperConfiguration zookeeperConfiguration = new ZookeeperConfiguration("127.0.0.1:2181", "my-job");
        this.registryCenter = new ZookeeperRegistryCenter(zookeeperConfiguration);
        this.registryCenter.init();
    }

    private static LiteJobConfiguration.Builder simpleJobConfigBuilder(String jobName,
                                                                       Class<? extends SimpleJob> jobClass,
                                                                       int shardTotalCount,
                                                                       String cron,
                                                                       String id) {
        return LiteJobConfiguration.newBuilder(new SimpleJobConfiguration(
                JobCoreConfiguration.newBuilder(jobName,cron,shardTotalCount).jobParameter(id).build(),jobClass.getCanonicalName()
        ));
    }

    /**
     * 添加一个定时任务
     */
    public void addJob(String jobName ,String cron, Integer shardTotalCount,String className) {
        JobCoreConfiguration jobCoreConfiguration =
                JobCoreConfiguration.newBuilder(jobName, cron, shardTotalCount).build();
        SimpleJobConfiguration simpleJobConfiguration = new SimpleJobConfiguration(jobCoreConfiguration, className);
        //启动任务
        new JobScheduler(registryCenter, LiteJobConfiguration.newBuilder(simpleJobConfiguration).build()).init();

    }

    /**
     * 更新定时任务
     * @param jobName
     * @param cron
     */
    public void updateJob(String jobName, String cron) {
        JobRegistry.getInstance().getJobScheduleController(jobName).rescheduleJob(cron);
    }

    /**
     * 删除定时任务
     * @param jobName
     */
    public void removeJob(String jobName){
        JobRegistry.getInstance().getJobScheduleController(jobName).shutdown();
    }
}