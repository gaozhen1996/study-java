package com.gz.javastudy;

import com.gz.javastudy.backupJob.BackupJob;
import com.gz.javastudy.core.JobDef;
import com.gz.javastudy.testJob.TestJob1;
import com.gz.javastudy.testJob.TestJob2;
import com.gz.javastudy.util.JdbcUtil;
import com.gz.javastudy.util.JdbcUtilSQLiteImpl;
import com.sun.xml.internal.bind.v2.model.impl.ModelBuilder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        ElasticJobHandler handler = new ElasticJobHandler();
        JdbcUtil jdbcUtil = new JdbcUtilSQLiteImpl();
        Scanner scanner = new Scanner(System.in);
        while (true){
            System.out.print("> ");
            String line = scanner.nextLine();
            String[] vars = line.split(" ");
            if (vars.length<=0){
                continue;
            }
            List<JobDef> jobDefs = jdbcUtil.convertList(jdbcUtil.executeQuery("select * from job_def"), JobDef.class);
            String var1 = vars[0].toUpperCase();
            switch (var1){
                case "SELECT":
                    int i = 1;
                    for(JobDef jobDef:jobDefs){
                        System.out.println("【"+i+"】"+jobDef);
                        i++;
                    }
                    break;
                case "ADD":
                    JobDef mode = jobDefs.get(Integer.parseInt(vars[1])-1);
                    handler.addJob(mode.getName(),mode.getCron(),mode.getShardTotalCount(),mode.getClassName());
                    break;
                case "UPDATE":
                    mode = jobDefs.get(Integer.parseInt(vars[1])-1);
                    handler.updateJob(mode.getName(),mode.getCron());
                    break;
                case "DELETE":
                    mode = jobDefs.get(Integer.parseInt(vars[1])-1);
                    handler.removeJob(mode.getName());
                default:continue;
            }
        }
    }

    static class Mode{
        String name;
        String cron;
        int shardTotalCount;
        String className;
        public Mode(String name,String cron,int shardTotalCount,String className){
            this.name = name;
            this.cron = cron;
            this.shardTotalCount = shardTotalCount;
            this.className = className;
        }
        public Mode(){
        }

        @Override
        public String toString() {
            return "Mode{" +
                    "name='" + name + '\'' +
                    ", cron='" + cron + '\'' +
                    ", shardTotalCount=" + shardTotalCount +
                    ", className='" + className + '\'' +
                    '}';
        }
    }


}
