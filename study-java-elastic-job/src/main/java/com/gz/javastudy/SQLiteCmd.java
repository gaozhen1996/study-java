package com.gz.javastudy;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;

public class SQLiteCmd {
    public static void main(String[] args) {
        new SQLiteCmd().start();
    }


    public void start() {
        Scanner scanner = new Scanner(System.in);
        try {
            // 加载驱动,连接sqlite的jdbc
            Class.forName("org.sqlite.JDBC");
            // 连接数据库how2j.db,不用手动创建。。。
            Connection connection = DriverManager.getConnection("jdbc:sqlite::resource:db/gz.db");
            // 创建连接对象，是Java的一个操作数据库的重要接口
            Statement statement = connection.createStatement();

            for (int i=0;i<1;){
                System.out.print("> ");
                String line = scanner.nextLine();
                String[] vars = line.split(" ");
                if (vars.length<=0){
                    continue;
                }
                //判断SQL语句类型
                String var1 = vars[0].toUpperCase();
                if (var1.equals("EXIT")){
                    break;
                }
                switch (var1){
                    case "SELECT":select(statement,line);break;
                    case "UPDATE":
                    case "INSERT":
                    case "DELETE":update(statement,line);break;
                    default:execute(statement,line);
                }
            }

            statement.close();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void select (Statement statement,String sql){
        try {
            List<Map<String, Object>> list = new ArrayList<>();
            //执行SQL，获取结果集
            ResultSet rs = statement.executeQuery(sql);
            int count = rs.getMetaData().getColumnCount();
            while (rs.next()) {
                Map<String, Object> map = new HashMap<String, Object>();
                for (int i = 0; i < count; i++) {
                    Object ob = rs.getObject(i + 1);
                    String key = rs.getMetaData().getColumnName(i + 1);
                    map.put(key, ob);

                }
                list.add(map);
            }
            rs.close();
            //打印
            for(Map<String, Object> map:list){
                System.out.println(map);
            }
            System.out.println("一共"+list.size()+"行");
        }catch (Exception e){
            System.out.println(e.getMessage());
        }

    }

    private void update(Statement statement,String sql)throws Exception{
        try {
            int count = statement.executeUpdate(sql);
            System.out.println("> 影响"+count+"行");
        }catch (Exception e){
            System.out.println(e.getMessage());
        }

    }

    private void execute(Statement statement,String sql)throws Exception{
        try {
            statement.execute(sql);
            System.out.println("> 执行完成");
        }catch (Exception e){
            System.out.println(e.getMessage());
        }

    }

}
