package com.gz.javastudy.util;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public interface JdbcUtil {
    /**
     * 执行更新
     * @param sql
     * @param obj
     */
    void executeUpdate(String sql, Object... obj);

    /**
     * 执行查询
     * @param sql
     * @param obj
     * @return
     */
    List<Map<String, Object>> executeQuery(String sql, Object... obj);

    default <T> List<T> convertList(List<Map<String, Object>> source, Class<T> target){
        List<T> result = new ArrayList<>();
        source.forEach(obj -> {
            try {
                T t = target.newInstance();
                for (Field field:t.getClass().getDeclaredFields()) {
                    Object value = obj.get(field.getName());
                    field.setAccessible(true);
                    field.set(t, value);
                }
                result.add(t);
            }  catch (Exception e) {
                e.printStackTrace();
            }
        });

        return result;
    }
}
