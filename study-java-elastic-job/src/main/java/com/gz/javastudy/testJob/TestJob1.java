package com.gz.javastudy.testJob;

import com.dangdang.ddframe.job.api.ShardingContext;
import com.dangdang.ddframe.job.api.simple.SimpleJob;

/**
 *  测试job1
 * @author gaozhen
 */
public class TestJob1 implements SimpleJob {

    @Override
    public void execute(ShardingContext shardingContext){
        System.out.println("test job 1");
    }


}