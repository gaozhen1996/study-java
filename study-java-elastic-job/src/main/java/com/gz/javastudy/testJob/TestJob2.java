package com.gz.javastudy.testJob;

import com.dangdang.ddframe.job.api.ShardingContext;
import com.dangdang.ddframe.job.api.simple.SimpleJob;

/**
 *  测试job2
 * @author gaozhen
 */
public class TestJob2 implements SimpleJob {

    @Override
    public void execute(ShardingContext shardingContext){
        System.out.println("test job 2");
    }


}