package com.gz.javastudy.backupJobyLock;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;

public class RedissonFactory {

    private static RedissonClient redissonClient;

    public static RedissonClient getRedissonClient() {
        synchronized (RedissonFactory.class) {
            if(redissonClient==null) {
                Config config = new Config();
                //单机redis
                config.useSingleServer().setAddress("redis://127.0.0.1:6379");
                RedissonClient redisson = Redisson.create(config);
                redissonClient = redisson;
            }
        }
        return redissonClient;
    }

}