package com.gz.javastudy.backupJobyLock;

import com.dangdang.ddframe.job.api.ShardingContext;
import com.dangdang.ddframe.job.api.simple.SimpleJob;
import com.dangdang.ddframe.job.config.JobCoreConfiguration;
import com.dangdang.ddframe.job.config.simple.SimpleJobConfiguration;
import com.dangdang.ddframe.job.lite.api.JobScheduler;
import com.dangdang.ddframe.job.lite.config.LiteJobConfiguration;
import com.dangdang.ddframe.job.reg.base.CoordinatorRegistryCenter;
import com.dangdang.ddframe.job.reg.zookeeper.ZookeeperConfiguration;
import com.dangdang.ddframe.job.reg.zookeeper.ZookeeperRegistryCenter;
import com.gz.javastudy.util.JdbcUtil;
import com.gz.javastudy.util.JdbcUtilSQLiteImpl;
import org.redisson.api.RLock;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;


public class BackupJobyLock implements SimpleJob {

        @Override
        public void execute(ShardingContext shardingContext) {
            String date = new SimpleDateFormat("YYYY:MM:dd HH:mm:ss").format(new Date());
            System.out.println("时间：" + date + " 执行任务：");
            System.out.println("总共分片：" + shardingContext.getShardingTotalCount());
            System.out.println("当前分片：" + shardingContext.getShardingItem());
            /*
            从resume数据表查找1条未归档的数据，将其归档到resume_bak
            表，并更新状态为已归档（不删除原数据）
            */
            RLock rLock = RedissonFactory.getRedissonClient().getLock("resume");
            try {
                rLock.lock();
                // 查询出⼀条数据
                String selectSql = "select * from resume where state='未归档' limit 1";
                JdbcUtil jdbcUtil = new JdbcUtilSQLiteImpl();
                List<Map<String, Object>> list = jdbcUtil.executeQuery(selectSql);
                if (list == null || list.size() == 0) {
                    System.out.println();
                    return;
                }
                Map<String, Object> stringObjectMap = list.get(0);
                String name = (String) stringObjectMap.get("name");
                String education = (String) stringObjectMap.get("education");
                // 打印出这条记录
                int id = (int) stringObjectMap.get("id");
                System.out.println("======>>>id：" + id + " name：" + name + " education：" + education);
                // 更改状态
                String updateSql = "update resume set state='已归档' where id=?";
                jdbcUtil.executeUpdate(updateSql, id);
                // 归档这条记录
                String insertSql = "insert into resume_bak select * from resume where id=?";
                jdbcUtil.executeUpdate(insertSql, id);
            } finally {
                rLock.unlock();
            }

        }


        public static void main(String[] args) {
            //配置分布式Zookeeper分布式协调中心
            ZookeeperConfiguration zookeeperConfiguration = new ZookeeperConfiguration("127.0.0.1:2181", "elastic-job");
            CoordinatorRegistryCenter coordinatorRegistryCenter = new ZookeeperRegistryCenter(zookeeperConfiguration);
            coordinatorRegistryCenter.init();

            //配置任务
            JobCoreConfiguration jobCoreConfiguration =
                    JobCoreConfiguration.newBuilder("archive-job", "/5 * * * * ?", 2).build();
            SimpleJobConfiguration simpleJobConfiguration = new SimpleJobConfiguration(jobCoreConfiguration, BackupJobyLock.class.getName());
            //启动任务
            new JobScheduler(coordinatorRegistryCenter, LiteJobConfiguration.newBuilder(simpleJobConfiguration).build()).init();
        }

}