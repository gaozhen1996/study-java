package com.gz.javastudy.zappdc.config;

import com.alibaba.druid.pool.DruidDataSource;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.InputStreamResource;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.io.InputStream;

/**
 * @author gaozhen
 */
@Component
public class MyBatisUseSpringConfig {
	
	public DataSource dataSource() {
		DruidDataSource dataSource = new DruidDataSource();
		/**
		 * 配置数据源
		 */
		//加载驱动
		dataSource.setDriverClassName("com.mysql.jdbc.Driver");
		//数据库路径
		String url = "jdbc:mysql://mysql.server:3306/dc?characterEncoding=utf8&useSSL=false";
		dataSource.setUrl(url);
		dataSource.setUsername("dc");
		dataSource.setPassword("123456");

		/**
		 * 可选配置
		 */
		//初始连接数，默认0
		dataSource.setInitialSize(10);
		//最大连接数，默认8
		dataSource.setMaxActive(30);
		//最小闲置数
		dataSource.setMinIdle(10);
		//获取连接的最大等待时间，单位毫秒
		dataSource.setMaxWait(2000);
		//缓存PreparedStatement，默认false
		dataSource.setPoolPreparedStatements(true);
		//缓存PreparedStatement的最大数量，默认-1（不缓存）。大于0时会自动开启缓存PreparedStatement，所以可以省略上一句代码
		dataSource.setMaxOpenPreparedStatements(20);

		return dataSource;
	}

	@Bean
	public SqlSessionFactory sqlSessionFactory() throws Exception {
		SqlSessionFactoryBean factoryBean = new SqlSessionFactoryBean();
	  	factoryBean.setDataSource(dataSource());
	  	//设置mybatis配置文件路径
		String resource = "mybatis/dc-mybatis-config.xml";
		InputStream inputStream = Resources.getResourceAsStream(resource);
		InputStreamResource inputStreamResource = new InputStreamResource(inputStream);
		factoryBean.setConfigLocation(inputStreamResource);
		SqlSessionFactory sqlSessionFactory = factoryBean.getObject();
		assert sqlSessionFactory != null;
	  	return sqlSessionFactory;
	}
}
