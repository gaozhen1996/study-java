package com.gz.javastudy.zappdc;


import com.gz.javastudy.zappdc.toolkit.SpringUtil;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author gaozhen
 */
@Configuration
@ComponentScan("com.gz.javastudy.zappdc")
@MapperScan(basePackages ="com.gz.javastudy.zappdc.repository")
public class DcKafkaApplication {
    private static volatile boolean running = true;

    public static void main(String[] args) {
        try {
            //此处扫描包名，不能包含代理类的包名
            //只需要包含service，监听器的包名即可
            AnnotationConfigApplicationContext context =
                    new AnnotationConfigApplicationContext();
            context.register(DcKafkaApplication.class);
            SpringUtil.setApplicationContext(context);
            context.refresh();
            //在jvm中增加一个关闭的钩子,当jvm关闭的时候，
            //会执行系统中已经设置的所有通过方法addShutdownHook
            //添加的钩子，当系统执行完这些钩子后，jvm才会关闭。
            Runtime.getRuntime().addShutdownHook(new Thread(() -> {
                try {
                    context.stop();
                } catch (Throwable t) {
                    t.printStackTrace();
                }
                synchronized (DcKafkaApplication.class) {
                    running = false;
                    DcKafkaApplication.class.notify();
                }
            }));
            context.start();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);

        }
        synchronized (DcKafkaApplication.class) {
            while (running) {
                try {
                    DcKafkaApplication.class.wait();
                } catch (Throwable e) {
                }
            }
        }


    }
}
