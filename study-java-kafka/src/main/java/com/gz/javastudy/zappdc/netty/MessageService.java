package com.gz.javastudy.zappdc.netty;

import com.alibaba.fastjson.JSONObject;
import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.PooledByteBufAllocator;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.LengthFieldPrepender;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;


import java.nio.charset.Charset;
import java.util.concurrent.Executors;

/**
 * @author gaozhen
 */
@Service
public class MessageService{
    Logger logger = Logger.getLogger(MessageService.class);
    private static EventLoopGroup group;
    private static Bootstrap bootstrap;
    private static ChannelFuture future;

    public void sendMessage(String topicType,JSONObject message){
        if (future == null){
            synchronized (MessageService.class){
                getBootstrap();
                System.out.println(future);

            }
        }
        message.put("topic_type",topicType);
        logger.info("发送消息:"+message);
        future.channel().writeAndFlush(message.toJSONString());
    }


    private static Bootstrap getBootstrap(){
        //客户端启动辅助类
        bootstrap = new Bootstrap();
        //开启一个线程组
        group = new NioEventLoopGroup();
        //设置socket通道
        bootstrap.channel(NioSocketChannel.class);
        bootstrap.group(group);
        //设置内存分配器
        bootstrap.option(ChannelOption.ALLOCATOR, PooledByteBufAllocator.DEFAULT);
        final ClientHandler handler = new ClientHandler();
        //把handler加入到管道中
        bootstrap.handler(new ChannelInitializer<NioSocketChannel>() {
            @Override
            protected void initChannel(NioSocketChannel ch)
                    throws Exception {
                ch.pipeline().addLast(new LengthFieldBasedFrameDecoder(Integer.MAX_VALUE, 0, 8, 0, 8));
                ch.pipeline().addLast(new StringDecoder());
                ch.pipeline().addLast(handler);
                ch.pipeline().addLast(new LengthFieldPrepender(8, false));
                ch.pipeline().addLast(new StringEncoder(Charset.forName("utf-8")));
            }
        });

        try {
            future = null;
            future = bootstrap.connect("127.0.0.1",8088).sync();
        }catch (Exception e){
            e.printStackTrace();
        }

        return bootstrap;
    }

    public static class ClientHandler extends ChannelInboundHandlerAdapter {
        private static final Logger logger = Logger.getLogger(ClientHandler.class);

        @Override
        public void channelRead(ChannelHandlerContext ctx, Object msg)
                throws Exception {
            logger.info("接收消息：" + msg.toString());
        }

        @Override
        public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
            /**
             * exceptionCaught() 事件处理方法是当出现 Throwable 对象才会被调用，即当 Netty 由于 IO
             * 错误或者处理器在处理事件时抛出的异常时。在大部分情况下，捕获的异常应该被记录下来 并且把关联的 channel
             * 给关闭掉。然而这个方法的处理方式会在遇到不同异常的情况下有不 同的实现，比如你可能想在关闭连接之前发送一个错误码的响应消息。
             */
            // 出现异常就关闭
            logger.error("连接中断");
            //重连
            Executors.newSingleThreadExecutor().submit(new Runnable() {
                @Override
                public void run() {
                    while (true) {
                        synchronized (MessageService.class){
                            getBootstrap();
                            if (future!=null) {
                                logger.info("重连成功");
                                break;
                            }
                        }
                        try {
                            Thread.sleep(3000);
                        }catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
            ctx.close();
        }
    }


}
